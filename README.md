# Examination Service

Stores examinations, offers CRUD functionality.

***

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone with `git clone --recurse-submodules`

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd examination-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Create a `.env` file:

```bash
cp sample.env .env # modify if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build
```

If only DB server (incl. PGADMIN) should be started via compose

```bash
docker-compose up --build -d pgadmin

./run.sh --port=5000 --reload
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black .
pycodestyle examination_service tests
pylint examination_service tests
pytest tests/v1 tests/v3 --maxfail=1
```

### Recommended VSCode Settings

In `.vscode/settings.json`.

```json
{
    "python.linting.pylintEnabled": true,
    "python.linting.pycodestyleEnabled": false,
    "python.formatting.provider": "black"
}
```

## Usage

See:
- `http://localhost:5000/docs`
- `http://localhost:5000/v1/docs`
- `http://localhost:5000/v3/docs`
