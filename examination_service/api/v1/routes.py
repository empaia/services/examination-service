from fastapi import Path
from pydantic import UUID4

from ...models.v1.commons import Id, Message
from ...models.v1.examination import (
    Examination,
    ExaminationList,
    ExaminationQuery,
    PostExamination,
    PostScope,
    PutExaminationState,
    Scope,
    ScopeToken,
)
from ...singletons import settings
from .clients.db_clients import db_clients
from .singletons import access_token_tools


def add_routes(app, late_init):
    @app.get(
        "/examinations",
        responses={
            200: {"model": ExaminationList},
            500: {"model": Message, "description": "No examinations could be retrieved"},
        },
    )
    async def _(
        skip: int = None,
        limit: int = None,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.get_examinations(query=None, skip=skip, limit=limit)

    @app.post(
        "/examinations",
        status_code=201,
        responses={
            201: {"model": Examination},
            409: {
                "model": Message,
                "description": "Examination could not be created. Only one open examination per case allowed.",
            },
            500: {"model": Message, "description": "Examination could not be created"},
        },
    )
    async def _(
        examination: PostExamination,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.add_examination(examination=examination)

    @app.put(
        "/examinations/query",
        responses={
            200: {"model": ExaminationList},
            500: {"model": Message, "description": "No examinations could be retrieved"},
        },
    )
    async def _(
        query: ExaminationQuery,
        skip: int = None,
        limit: int = None,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.get_examinations(query=query, skip=skip, limit=limit)

    @app.get(
        "/examinations/{ex_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        ex_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.get_examination(ex_id=ex_id)

    @app.get(
        "/jobs/{job_id}/examinations",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        job_id: Id,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.get_examination_by_job_id(job_id=job_id)

    @app.put(
        "/examinations/{ex_id}/state",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        ex_id: UUID4,
        state: PutExaminationState,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.set_state(ex_id=ex_id, put_state=state)

    @app.put(
        "/examinations/{ex_id}/apps/{app_id}/add",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            405: {
                "model": Message,
                "description": "App with given app_id already in examination",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
    )
    async def _(
        ex_id: UUID4,
        app_id: Id,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.add_app(ex_id=ex_id, app_id=app_id)

    @app.put(
        "/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            405: {
                "model": Message,
                "description": "Job with given job_id already in examination",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
    )
    async def _(
        ex_id: UUID4,
        app_id: Id,
        job_id: Id,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.add_job(ex_id=ex_id, app_id=app_id, job_id=job_id)

    @app.delete(
        "/examinations/{ex_id}/jobs/{job_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
    )
    async def _(
        ex_id: UUID4,
        job_id: Id,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.delete_job(ex_id=ex_id, job_id=job_id)

    @app.post(
        "/scopes",
        responses={
            201: {"model": Scope},
            400: {
                "model": Message,
                "description": "The scope exists already, or the examination does not exist or the app "
                "was not added to the examination",
            },
        },
    )
    async def _(
        scope: PostScope,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.scope.create_scope(str(scope.examination_id), scope.app_id, scope.user_id)

    @app.put(
        "/scopes",
        responses={
            200: {"model": Scope},
            404: {
                "model": Message,
                "description": "The scope was not found",
            },
        },
    )
    async def _(
        scope: PostScope,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.scope.get_scope(str(scope.examination_id), scope.app_id, scope.user_id)

    @app.get(
        "/public-key",
        response_model=bytes,
        responses={
            200: {"description": "Public key for validating Access Tokens created for Scopes"},
        },
    )
    async def _() -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Scopes."""
        return access_token_tools.public_key

    @app.get(
        "/scopes/{scope_id}/token",
        response_model=ScopeToken,
        responses={
            200: {"description": "The newly created Access Token"},
            404: {"description": "Scope not found"},
        },
    )
    async def _(
        scope_id: UUID4 = Path(..., description="The Id of the Scope to create the Token for"),
    ) -> ScopeToken:
        """Create and return an Access-Token that is needed by the workbench-service to validate scope requests."""
        # Query the scope only to check if it exists
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            scope = await clients.scope.get_scope_by_id(str(scope_id))
            assert scope is not None
            assert scope_id == scope.id
        access_token = access_token_tools.create_token(
            subject=str(scope_id), expires_after_seconds=settings.scope_token_exp
        )
        return ScopeToken(access_token=access_token)

    @app.get(
        "/scopes/{scope_id}",
        responses={
            200: {"model": Scope},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.scope.get_scope_by_id(str(scope_id))
