from http import HTTPStatus

from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ....models.v1.commons import Id
from ....models.v1.examination import Scope
from ....singletons import logger
from .examination_client import ExaminationClient


class ScopeClient:
    def __init__(self, conn):
        self.conn = conn
        self.examination_client = ExaminationClient(conn)

    @staticmethod
    def _sql_scope_as_json_insert():
        return """json_build_object(
            'id', id,
            'examination_id', examination_id,
            'app_id', app_id,
            'user_id', user_id,
            'created_at', EXTRACT(epoch FROM created_at)::int
        )"""

    @staticmethod
    def _sql_scope_as_json_select():
        return """json_build_object(
            'id', s.id,
            'examination_id', s.examination_id,
            'app_id', s.app_id,
            'user_id', s.user_id,
            'created_at', EXTRACT(epoch FROM s.created_at)::int
        )"""

    async def _validate_create_scope_request(self, examination_id: Id, app_id: Id):
        raw_examination = await self.examination_client.get_raw_examination(examination_id)
        if raw_examination is None:
            raise HTTPException(
                HTTPStatus.BAD_REQUEST, f"Cannot create scope, because no examination with id {examination_id} exists"
            )
        if app_id not in raw_examination["apps"]:
            raise HTTPException(
                HTTPStatus.BAD_REQUEST,
                f"Cannot create scope, because app with id {app_id} has not been added to "
                f"examination with id {examination_id}",
            )

    async def get_scope_by_id(self, scope_id: Id) -> Scope:
        sql = f"""
        SELECT
        {self._sql_scope_as_json_select()}
        FROM scopes as s
        WHERE s.id = $1;
        """
        logger.debug(sql)
        try:
            raw_scope = await self.conn.fetchrow(sql, scope_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        if raw_scope is None:
            raise HTTPException(HTTPStatus.NOT_FOUND, f"Scope with id {scope_id} not found")
        return Scope.model_validate(raw_scope["json_build_object"])

    async def _query_raw_scope(self, sql: str, examination_id: Id, app_id: Id, user_id: Id) -> dict:
        row = await self.conn.fetchrow(sql, examination_id, app_id, user_id)
        raw_scope = None if row is None else row["json_build_object"]
        return raw_scope

    async def get_scope(self, examination_id: Id, app_id: Id, user_id: Id) -> Scope:
        sql = f"""
        SELECT
        {self._sql_scope_as_json_select()}
        FROM scopes as s
        WHERE s.examination_id = $1 AND s.app_id = $2 AND s.user_id = $3;
        """
        logger.debug(sql)
        try:
            raw_scope = await self._query_raw_scope(sql, examination_id, app_id, user_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        return None if raw_scope is None else Scope.model_validate(raw_scope)

    async def _insert_scope(self, examination_id: Id, app_id: Id, user_id: Id):
        sql = f"""
        INSERT INTO scopes(examination_id, app_id, user_id, created_at)
        VALUES(
            $1,
            $2,
            $3,
            current_timestamp
        )
        ON CONFLICT (examination_id, app_id, user_id) DO NOTHING
        RETURNING {self._sql_scope_as_json_insert()};
        """
        logger.debug(sql)
        return await self._query_raw_scope(sql, examination_id, app_id, user_id)

    async def _add_scope(self, examination_id: Id, app_id: Id, user_id: Id) -> Scope:
        try:
            raw_scope = await self._insert_scope(examination_id, app_id, user_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        if raw_scope is None:
            raise HTTPException(
                HTTPStatus.BAD_REQUEST,
                f"Scope for examination {examination_id}, app {app_id}, and " "user {user_id} was already created.",
            )
        return Scope.model_validate(raw_scope)

    async def create_scope(self, examination_id: UUID4, app_id: Id, user_id: Id) -> Scope:
        await self._validate_create_scope_request(str(examination_id), app_id)
        return await self._add_scope(str(examination_id), app_id, user_id)
