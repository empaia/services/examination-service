from asyncpg import Connection

from ....db import set_type_codecs
from .examination_client import ExaminationClient
from .scope_client import ScopeClient


class Clients:
    def __init__(self, conn):
        self.examination = ExaminationClient(conn=conn)
        self.scope = ScopeClient(conn=conn)


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return Clients(conn=conn)
