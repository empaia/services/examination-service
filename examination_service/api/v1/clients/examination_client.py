from typing import List

from asyncpg.exceptions import PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from .... import __version__ as version
from ....models.v1.commons import Id
from ....models.v1.examination import (
    Examination,
    ExaminationApp,
    ExaminationList,
    ExaminationQuery,
    ExaminationState,
    PostExamination,
    PutExaminationState,
)
from ....singletons import logger


class ExaminationClient:
    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _sql_mapping_as_json_insert():
        return """json_build_object(
            'id', id,
            'case_id', case_id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'state', state,
            'created_at', EXTRACT(epoch FROM created_at)::int,
            'updated_at', EXTRACT(epoch FROM updated_at)::int,
            'apps', ARRAY[]::text[]
        )"""

    @staticmethod
    def _sql_mapping_as_json_select():
        return """json_build_object(
            'id', e.id,
            'case_id', e.case_id,
            'creator_id', e.creator_id,
            'creator_type', e.creator_type,
            'state', e.state,
            'created_at', EXTRACT(epoch FROM e.created_at)::int,
            'updated_at', EXTRACT(epoch FROM e.updated_at)::int,
            'apps', COALESCE(json_agg(DISTINCT a.app_id) FILTER (WHERE a.ex_id IS NOT NULL), '[]')
        )"""

    async def _accumulate_examination(self, raw_examination) -> Examination:
        apps = []
        for app_id in raw_examination["apps"]:
            apps.append(await self.get_examination_app(raw_examination["id"], app_id))
        raw_examination["apps"] = apps

        return Examination.model_validate(raw_examination)

    async def get_examinations(
        self, query: ExaminationQuery = None, skip: int = None, limit: int = None
    ) -> ExaminationList:
        if query is None:
            query = ExaminationQuery()

        where_terms = []
        args = []
        var_counter = 1
        if query.cases is not None:
            where_terms.append(f"e.case_id = ANY(${var_counter})")
            args.append(query.cases)
            var_counter += 1
        if query.creators is not None:
            where_terms.append(f"e.creator_id = ANY(${var_counter})")
            args.append(query.creators)
            var_counter += 1
        if query.creator_types is not None:
            where_terms.append(f"e.creator_type = ANY(${var_counter})")
            args.append(query.creator_types)
            var_counter += 1
        args.extend([limit, skip])

        count_sql = f"""
        SELECT COUNT(*) AS count
        FROM examinations as e
        {await self.concat_sql_statemment(where_terms, prefix="WHERE ", separator=" AND ")}
        """

        logger.debug(count_sql)

        sql = f"""
        SELECT
        {ExaminationClient._sql_mapping_as_json_select()}
        FROM examinations as e
        LEFT JOIN examinations_apps as a
            ON a.ex_id = e.id
        {await self.concat_sql_statemment(where_terms, prefix="WHERE ", separator=" AND ")}
        GROUP BY e.id
        ORDER BY e.updated_at DESC
        LIMIT ${var_counter}
        OFFSET ${var_counter + 1};
        """

        logger.debug(sql)

        try:
            raw_examinations_count = await self.conn.fetchrow(count_sql, *args[:-2])
            if raw_examinations_count["count"] == 0:
                return ExaminationList(item_count=0, items=[])
            raw_examinations = await self.conn.fetch(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e) from e

        if raw_examinations is None:
            return ExaminationList(item_count=0, items=[])

        if len(raw_examinations) == 0:
            return ExaminationList(item_count=0, items=[])
        else:
            examinations = []
            for r_ex in raw_examinations:
                examinations.append(await self._accumulate_examination(r_ex["json_build_object"]))
            return ExaminationList.model_validate(
                {"item_count": raw_examinations_count["count"], "items": examinations}
            )

    async def get_raw_examination(self, ex_id: UUID4) -> dict:
        sql = f"""
        SELECT
        {ExaminationClient._sql_mapping_as_json_select()}
        FROM examinations as e
        LEFT JOIN examinations_apps as a
            ON a.ex_id = e.id
        WHERE e.id = $1
        GROUP BY e.id;
        """

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, ex_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e
        return None if row is None else row["json_build_object"]

    async def get_examination(self, ex_id: UUID4) -> Examination:
        raw_examination = await self.get_raw_examination(ex_id)
        if raw_examination is None:
            raise HTTPException(404, f"Examination with id {ex_id} not found")
        else:
            return await self._accumulate_examination(raw_examination)

    async def get_examination_app(self, ex_id: UUID4, app_id: Id) -> ExaminationApp:
        sql = """
        SELECT
        j.job_id
        FROM examinations_jobs as j
        WHERE j.ex_id = $1 AND j.app_id = $2
        """

        logger.debug(sql)

        try:
            raw_app = await self.conn.fetch(sql, ex_id, app_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

        if raw_app is None:
            return None
        else:
            jobs_list = []
            for a in raw_app:
                jobs_list.append(a[0])
            return {"id": app_id, "jobs": jobs_list}

    async def get_examination_by_job_id(self, job_id: Id):
        sql = """
        SELECT ex_id
        FROM examinations_jobs
        WHERE job_id = $1;
        """

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

        if row is None:
            raise HTTPException(404, f"No examination found for job_id: '{job_id}'!")
        return await self.get_examination(row["ex_id"])

    async def add_examination(self, examination: PostExamination) -> Examination:
        sql = f"""
        INSERT INTO examinations(
            case_id,
            creator_id,
            creator_type,
            state,
            created_at,
            updated_at
        )
        VALUES(
            $1,
            $2,
            $3,
            'OPEN',
            current_timestamp,
            current_timestamp
        ) RETURNING {ExaminationClient._sql_mapping_as_json_insert()};
        """

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, examination.case_id, examination.creator_id, examination.creator_type)
        except UniqueViolationError as e:
            raise HTTPException(409, "There already is an open examination for this case") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

        if row is None:
            raise HTTPException(500, "Examination could not be created")

        return Examination.model_validate(row["json_build_object"])

    async def set_state(self, ex_id: UUID4, put_state: PutExaminationState) -> Examination:
        if await self.get_state(ex_id) == ExaminationState.CLOSED and put_state.state == ExaminationState.CLOSED:
            return await self.get_examination(ex_id=ex_id)
        sql = """
        UPDATE examinations
            SET state=$2,
                updated_at=current_timestamp
        WHERE id=$1;
        """

        logger.debug(sql)

        try:
            await self.conn.execute(sql, ex_id, put_state.state)
            return await self.get_examination(ex_id=ex_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

    async def get_state(self, ex_id: UUID4) -> str:
        examination = await self.get_examination(ex_id)
        if examination is None:
            raise HTTPException(404, "The resource was not found")
        return examination.state

    async def _set_updated_time(self, ex_id: UUID4) -> bool:
        sql = """
        UPDATE examinations
            SET updated_at=current_timestamp
        WHERE id=$1;
        """

        logger.debug(sql)

        try:
            await self.conn.execute(sql, ex_id)
            return True
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(400, "Examination could not be updated") from e

    async def add_app(self, ex_id: UUID4, app_id: Id) -> Examination:
        if await self.get_state(ex_id) == ExaminationState.CLOSED:
            raise HTTPException(423, "Examination has state 'CLOSED': app can not be added!")

        sql = """
        INSERT INTO examinations_apps(ex_id, app_id)
        VALUES($1, $2);
        """

        logger.debug(sql)

        try:
            await self.conn.execute(sql, ex_id, app_id)
            await self._set_updated_time(ex_id)
            return await self.get_examination(ex_id=ex_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(405, "App with given app_id already in examination.") from e

    async def add_job(self, ex_id: UUID4, app_id: Id, job_id: Id) -> Examination:
        if await self.get_state(ex_id) == ExaminationState.CLOSED:
            raise HTTPException(423, "Examination has state 'CLOSED': app can not be added!")
        if not await self.is_app_in_examination(ex_id, app_id):
            raise HTTPException(404, f"App with ID {app_id} does not exist for given examination")

        sql = """
        INSERT INTO examinations_jobs(ex_id, app_id, job_id)
        VALUES($1, $2, $3);
        """

        logger.debug(sql)

        try:
            await self.conn.execute(sql, ex_id, app_id, job_id)
            await self._set_updated_time(ex_id)
            return await self.get_examination(ex_id=ex_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(405, "Job with given job_id already in examination.") from e

    async def is_app_in_examination(self, ex_id: UUID4, app_id: Id):
        sql = """
        SELECT *
        FROM examinations_apps
        WHERE ex_id = $1 AND app_id = $2;
        """

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, ex_id, app_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

        if row is None:
            return False
        else:
            return True

    async def delete_job(self, ex_id: UUID4, job_id: Id):
        if await self.get_state(ex_id) == ExaminationState.CLOSED:
            raise HTTPException(423, "Examination has state 'CLOSED': job can not be deleted!")

        sql = """
        DELETE FROM examinations_jobs
        WHERE ex_id = $1 AND job_id = $2
        RETURNING job_id;
        """

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, ex_id, job_id)
            await self._set_updated_time(ex_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

        if row is None:
            raise HTTPException(404, f"No job with id '{job_id}' found in examination")
        return await self.get_examination(ex_id)

    async def concat_sql_statemment(self, terms: List[str], prefix: str, separator: str):
        if len(terms) == 0:
            return ""
        else:
            statement = separator.join(terms)
            return prefix + statement
