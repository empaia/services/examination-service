from fastapi import Response

from ...models.commons import ServiceStatus, ServiceStatusEnum
from .clients.db_clients import db_clients


def add_routes(app, late_init):
    @app.get(
        "/alive",
        tags=["server"],
        responses={
            200: {"model": ServiceStatus},
            500: {"model": ServiceStatus},
        },
    )
    async def _(
        response: Response,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            service_status = await clients.general.check_server_health()
            response.status_code = 200 if service_status.status == ServiceStatusEnum.OK else 500
            return service_status
