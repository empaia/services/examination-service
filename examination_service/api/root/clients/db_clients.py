from asyncpg import Connection

from ....db import set_type_codecs
from .general_client import GeneralClient


class Clients:
    def __init__(self, conn):
        self.general = GeneralClient(conn=conn)


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return Clients(conn=conn)
