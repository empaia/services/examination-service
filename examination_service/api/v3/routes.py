from fastapi import Path, Response
from pydantic import UUID4

from ...models.v3.commons import Id, Message
from ...models.v3.examination import (
    AppUiStorage,
    Examination,
    ExaminationList,
    ExaminationQuery,
    PostExamination,
    PostPreprocessingRequest,
    PostPreprocessingTrigger,
    PostScope,
    PreprocessingRequest,
    PreprocessingRequestList,
    PreprocessingRequestQuery,
    PreprocessingTrigger,
    PreprocessingTriggerList,
    PutExaminationState,
    PutPreprocessingRequestUpdate,
    Scope,
    ScopeList,
    ScopeQuery,
    ScopeToken,
)
from ...singletons import settings
from .clients.db_clients import db_clients
from .singletons import access_token_tools


def add_routes(app, late_init):
    @app.get(
        "/examinations",
        responses={
            200: {"model": ExaminationList},
            500: {"model": Message, "description": "No examinations could be retrieved"},
        },
    )
    async def _(
        skip: int = None,
        limit: int = None,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.get_examinations(query=None, skip=skip, limit=limit)

    @app.put(
        "/examinations",
        status_code=201,
        responses={
            200: {"model": Examination, "description": "Existing open examination"},
            201: {"model": Examination, "description": "Created open examination"},
            500: {"model": Message, "description": "Examination could not be created"},
        },
    )
    async def _(
        examination: PostExamination,
        response: Response,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            examination, created = await clients.examination.create_or_get_open_examination(examination=examination)
            response.status_code = 201 if created else 200
            return examination

    @app.put(
        "/examinations/query",
        responses={
            200: {"model": ExaminationList},
            500: {"model": Message, "description": "No examinations could be retrieved"},
        },
    )
    async def _(
        query: ExaminationQuery,
        skip: int = None,
        limit: int = None,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.get_examinations(query=query, skip=skip, limit=limit)

    @app.get(
        "/examinations/{ex_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        ex_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.get_examination(ex_id=ex_id)

    @app.put(
        "/examinations/{ex_id}/state",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            409: {
                "model": Message,
                "description": "Only one open examination allowed per case / app pair",
            },
        },
    )
    async def _(
        ex_id: UUID4,
        state: PutExaminationState,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.set_state(ex_id=ex_id, put_state=state)

    @app.put(
        "/examinations/{ex_id}/jobs/{job_id}/add",
        responses={
            200: {"model": Examination, "description": "If job was already in examination."},
            201: {"model": Examination, "description": "If job was newly added to examination."},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
    )
    async def _(
        ex_id: UUID4,
        job_id: Id,
        response: Response,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            examination, job_added = await clients.examination.add_job(ex_id=ex_id, job_id=job_id)
        response.status_code = 201 if job_added else 200
        return examination

    @app.delete(
        "/examinations/{ex_id}/jobs/{job_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
    )
    async def _(
        ex_id: UUID4,
        job_id: Id,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.examination.delete_job(ex_id=ex_id, job_id=job_id)

    @app.put(
        "/scopes",
        status_code=201,
        responses={
            200: {"model": Scope, "description": "Scope already exists. Returning existing."},
            201: {"model": Scope, "description": "Scope created."},
            404: {
                "model": Message,
                "description": "Could not create or get scope. No examination with given id exists.",
            },
        },
    )
    async def _(
        scope: PostScope,
        response: Response,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            response_scope = await clients.scope.get_scope(str(scope.examination_id), scope.user_id)
            status_code = 200
            if response_scope is None:
                response_scope = await clients.scope.create_scope(str(scope.examination_id), scope.user_id)
                status_code = 201
            response.status_code = status_code
            return response_scope

    @app.get(
        "/public-key",
        response_model=bytes,
        responses={
            200: {"description": "Public key for validating Access Tokens created for Scopes"},
        },
    )
    async def _() -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Scopes."""
        return access_token_tools.public_key

    @app.get(
        "/scopes/{scope_id}/token",
        response_model=ScopeToken,
        responses={
            200: {"description": "The newly created Access Token"},
            404: {"description": "Scope not found"},
        },
    )
    async def _(
        scope_id: UUID4 = Path(..., description="The Id of the Scope to create the Token for"),
    ) -> ScopeToken:
        """Create and return an Access-Token that is needed by the workbench-service to validate scope requests."""
        # Query the scope only to check if it exists
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            scope = await clients.scope.get_scope_by_id(str(scope_id))
            assert scope is not None
            assert scope_id == scope.id
        access_token = access_token_tools.create_token_from_dict(
            payload={"sub": str(scope_id)}, expires_after_seconds=settings.scope_token_exp
        )
        return ScopeToken(access_token=access_token)

    @app.get(
        "/scopes/{scope_id}",
        responses={
            200: {"model": Scope},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.scope.get_scope_by_id(str(scope_id))

    @app.put(
        "/scopes/query",
        responses={
            200: {"model": ScopeList},
            500: {"model": Message, "description": "No scopes could be retrieved"},
        },
    )
    async def _(
        query: ScopeQuery,
        skip: int = None,
        limit: int = None,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.scope.get_scopes(query=query, skip=skip, limit=limit)

    @app.get(
        "/scopes/{scope_id}/app-ui-storage/user",
        responses={
            200: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            scope = await clients.scope.get_scope_by_id(str(scope_id))
            examination = await clients.examination.get_examination(scope.examination_id)
            app_id = examination.app_id
            user_id = scope.user_id
            return await clients.user_app_ui_storage.get_user_app_storage(str(app_id), str(user_id))

    @app.get(
        "/scopes/{scope_id}/app-ui-storage/scope",
        responses={
            200: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            _scope = await clients.scope.get_scope_by_id(str(scope_id))
            return await clients.scope_app_ui_storage.get_scope_app_storage(str(scope_id))

    @app.put(
        "/scopes/{scope_id}/app-ui-storage/user",
        status_code=201,
        responses={
            201: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: UUID4,
        state: AppUiStorage,
        response: Response,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            scope = await clients.scope.get_scope_by_id(str(scope_id))
            examination = await clients.examination.get_examination(scope.examination_id)
            app_id = examination.app_id
            user_id = scope.user_id
            state = await clients.user_app_ui_storage.put_user_app_storage(str(app_id), str(user_id), state)
            response.status_code = 201
            return state

    @app.put(
        "/scopes/{scope_id}/app-ui-storage/scope",
        status_code=201,
        responses={
            201: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: UUID4,
        state: AppUiStorage,
        response: Response,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            _scope = await clients.scope.get_scope_by_id(str(scope_id))
            state = await clients.scope_app_ui_storage.put_scope_app_storage(str(scope_id), state)
            response.status_code = 201
            return state

    @app.post(
        "/preprocessing-triggers",
        status_code=201,
        responses={
            201: {"model": PreprocessingTrigger},
            409: {
                "model": Message,
                "description": "The trigger combination already exists",
            },
        },
    )
    async def _(
        trigger: PostPreprocessingTrigger,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.trigger.add_preprocessing_trigger(trigger)

    @app.get(
        "/preprocessing-triggers",
        responses={
            200: {"model": PreprocessingTriggerList},
        },
    )
    async def _():
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.trigger.get_preprocessing_triggers()

    @app.get(
        "/preprocessing-triggers/{trigger_id}",
        responses={
            200: {"model": PreprocessingTrigger},
            404: {"model": Message, "description": "The trigger does not exist"},
        },
    )
    async def _(
        trigger_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.trigger.get_preprocessing_trigger(trigger_id)

    @app.delete(
        "/preprocessing-triggers/{trigger_id}",
        status_code=204,
        responses={
            204: {"model": None},
            404: {"model": Message, "description": "The trigger does not exist"},
        },
    )
    async def _(
        trigger_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            await clients.trigger.delete_preprocessing_trigger(trigger_id)

    @app.post(
        "/preprocessing-requests",
        status_code=201,
        responses={
            201: {"model": PreprocessingRequest},
        },
    )
    async def _(
        request: PostPreprocessingRequest,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.request.add_preprocessing_request(request)

    @app.get(
        "/preprocessing-requests/{request_id}",
        responses={
            200: {"model": PreprocessingRequest},
            404: {"model": Message, "description": "The request does not exist"},
        },
    )
    async def _(
        request_id: UUID4,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.request.get_preprocessing_request(request_id)

    @app.put(
        "/preprocessing-requests/query",
        status_code=200,
        responses={
            200: {"model": PreprocessingRequestList},
        },
    )
    async def _(
        query: PreprocessingRequestQuery,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.request.query_preprocessing_requests(query)

    @app.put(
        "/preprocessing-requests/{request_id}/process",
        status_code=200,
        responses={
            200: {"model": PreprocessingRequest},
            404: {"model": Message, "description": "The request does not exist"},
            409: {"model": Message, "description": "The request has already been processed"},
        },
    )
    async def _(
        request_id: UUID4,
        update: PutPreprocessingRequestUpdate,
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.request.process_preprocessing_request(request_id, update.app_jobs)
