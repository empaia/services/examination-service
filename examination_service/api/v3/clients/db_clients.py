from asyncpg import Connection

from ....db import set_type_codecs
from .examination_client import ExaminationClient
from .request_client import PreprocessingRequestClient
from .scope_app_ui_storage_client import ScopeAppUiStorageClient
from .scope_client import ScopeClient
from .trigger_client import PreprocessingTriggerClient
from .user_app_ui_storage_client import UserAppUiStorageClient


class Clients:
    def __init__(self, conn):
        self.examination = ExaminationClient(conn=conn)
        self.scope = ScopeClient(conn=conn)
        self.trigger = PreprocessingTriggerClient(conn=conn)
        self.request = PreprocessingRequestClient(conn=conn)
        self.scope_app_ui_storage = ScopeAppUiStorageClient(conn=conn)
        self.user_app_ui_storage = UserAppUiStorageClient(conn=conn)


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return Clients(conn=conn)
