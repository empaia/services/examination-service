from typing import List


async def concat_sql_statemment(terms: List[str], prefix: str, separator: str):
    if len(terms) == 0:
        return ""
    else:
        statement = separator.join(terms)
        return prefix + statement
