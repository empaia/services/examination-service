from typing import Tuple

from asyncpg.exceptions import DataError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from .... import __version__ as version
from ....models.v3.commons import Id
from ....models.v3.examination import (
    Examination,
    ExaminationList,
    ExaminationQuery,
    ExaminationState,
    PostExamination,
    PutExaminationState,
)
from ....singletons import logger
from .utils import concat_sql_statemment


class ExaminationClient:
    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _sql_mapping_as_json_insert():
        return """json_build_object(
            'id', id,
            'case_id', case_id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'state', state,
            'created_at', EXTRACT(epoch FROM created_at)::int,
            'updated_at', EXTRACT(epoch FROM updated_at)::int,
            'app_id', app_id,
            'jobs', ARRAY[]::text[]
        )"""

    @staticmethod
    def _sql_mapping_as_json_select():
        return """json_build_object(
            'id', e.id,
            'case_id', e.case_id,
            'creator_id', e.creator_id,
            'creator_type', e.creator_type,
            'state', e.state,
            'created_at', EXTRACT(epoch FROM e.created_at)::int,
            'updated_at', EXTRACT(epoch FROM e.updated_at)::int,
            'app_id', e.app_id,
            'jobs', COALESCE(json_agg(DISTINCT j.job_id) FILTER (WHERE j.ex_id IS NOT NULL), '[]')
        )"""

    async def get_examinations(
        self, query: ExaminationQuery = None, skip: int = None, limit: int = None
    ) -> ExaminationList:
        if query is None:
            query = ExaminationQuery()

        where_terms = []
        args = []
        var_counter = 1
        jobs_join_sql = ""
        jobs_agg_sql = ""
        scopes_join_sql = ""
        scopes_agg_sql = ""
        if query.cases is not None:
            where_terms.append(f"e.case_id = ANY(${var_counter})")
            args.append(query.cases)
            var_counter += 1
        if query.creators is not None:
            where_terms.append(f"e.creator_id = ANY(${var_counter})")
            args.append(query.creators)
            var_counter += 1
        if query.creator_types is not None:
            where_terms.append(f"e.creator_type = ANY(${var_counter})")
            args.append(query.creator_types)
            var_counter += 1
        if query.apps is not None:
            where_terms.append(f"e.app_id = ANY(${var_counter})")
            args.append(query.apps)
            var_counter += 1
        if query.scopes is not None:
            where_terms.append(f"s.id = ANY(${var_counter})")
            args.append(query.scopes)
            var_counter += 1
            scopes_join_sql = """
            LEFT JOIN v3_scopes as s
                ON uuid(s.examination_id) = e.id
            """
            scopes_agg_sql = ",array_agg(s.id)"
        if query.jobs is not None:
            where_terms.append(f"j.job_id = ANY(${var_counter})")
            args.append(query.jobs)
            var_counter += 1
            jobs_join_sql = """
            LEFT JOIN v3_examinations_jobs as j
                ON j.ex_id = e.id
            """
            jobs_agg_sql = ",array_agg(j.job_id)"
        args.extend([limit, skip])

        sql = f"""
        WITH exas AS (
            SELECT
                ex.id,
                ex.case_id,
                ex.creator_id,
                ex.creator_type,
                ex.state,
                ex.created_at,
                ex.updated_at,
                ex.app_id
            FROM
                (
                    SELECT
                        e.*
                        {jobs_agg_sql}
                        {scopes_agg_sql}
                    FROM
                        v3_examinations AS e
                    {jobs_join_sql}
                    {scopes_join_sql}
                    {await concat_sql_statemment(where_terms, prefix="WHERE ", separator=" AND ")}
                    GROUP BY
                        e.id
                ) AS ex
            ORDER BY
                ex.updated_at DESC
        )
        SELECT
            count(exas.*) AS count,
            (
                SELECT
                    json_agg(e) AS items
                FROM
                    (
                        SELECT
                            {ExaminationClient._sql_mapping_as_json_select()} AS item
                        FROM
                            exas AS e
                            LEFT JOIN v3_examinations_jobs AS j
                                ON j.ex_id = e.id
                        GROUP BY
                            e.id,
                            e.app_id,
                            e.case_id,
                            e.creator_id,
                            e.creator_type,
                            e.state,
                            e.created_at,
                            e.updated_at
                        ORDER BY
                            e.updated_at DESC
                        LIMIT ${var_counter}
                        OFFSET ${var_counter + 1}
                    ) AS e
            )
        FROM
            exas;
        """
        logger.debug(sql)

        try:
            raw_examinations = await self.conn.fetchrow(sql, *args)
        except DataError as e:
            raise HTTPException(422, "ERROR: No valid UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.args) from e

        if raw_examinations is None:
            return ExaminationList(item_count=0, items=[])

        if len(raw_examinations) == 0:
            return ExaminationList(item_count=0, items=[])
        else:
            examinations = []
            if raw_examinations["items"]:
                for r_ex in raw_examinations["items"]:
                    examinations.append(
                        Examination.model_validate(r_ex["item"]),
                    )
            return ExaminationList.model_validate({"item_count": raw_examinations["count"], "items": examinations})

    async def get_raw_examination(self, ex_id: UUID4) -> Examination:
        sql = f"""
        SELECT
        {ExaminationClient._sql_mapping_as_json_select()}
        FROM v3_examinations as e
        LEFT JOIN v3_examinations_jobs as j
            ON j.ex_id = e.id
        WHERE e.id = $1
        GROUP BY e.id;
        """

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, ex_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e
        return None if row is None else row["json_build_object"]

    async def get_examination(self, ex_id: UUID4) -> Examination:
        raw_examination = await self.get_raw_examination(ex_id)
        if raw_examination is None:
            raise HTTPException(404, f"Examination with id {ex_id} not found")
        else:
            return Examination.model_validate(raw_examination)

    async def add_examination(self, examination: PostExamination) -> Examination:
        sql = self._get_creation_sql()

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(
                sql,
                examination.case_id,
                examination.creator_id,
                examination.creator_type,
                examination.app_id,
            )
        except UniqueViolationError as e:
            raise HTTPException(409, "There already is an open examination for this case and app") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

        if row is None:
            raise HTTPException(500, "Examination could not be created")

        return Examination.model_validate(row["json_build_object"])

    async def create_or_get_open_examination(self, examination: PostExamination) -> Tuple[Examination, bool]:
        sql = self._get_creation_sql()

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(
                sql,
                examination.case_id,
                examination.creator_id,
                examination.creator_type,
                examination.app_id,
            )
        except UniqueViolationError:
            query = ExaminationQuery(cases=[examination.case_id], apps=[examination.app_id])
            examination_list = await self.get_examinations(query=query)
            for examination in examination_list.items:
                if examination.state == ExaminationState.OPEN:
                    return examination, False
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"{e}") from e

        if row is None:
            raise HTTPException(500, "Examination could not be created")

        return Examination.model_validate(row["json_build_object"]), True

    @staticmethod
    def _get_creation_sql():
        return f"""
        INSERT INTO v3_examinations(
            case_id,
            creator_id,
            creator_type,
            app_id,
            state,
            created_at,
            updated_at
        )
        VALUES(
            $1,
            $2,
            $3,
            $4,
            'OPEN',
            current_timestamp,
            current_timestamp
        ) RETURNING {ExaminationClient._sql_mapping_as_json_insert()};
        """

    async def set_state(self, ex_id: UUID4, put_state: PutExaminationState) -> Examination:
        if await self.get_state(ex_id) == ExaminationState.CLOSED and put_state.state == ExaminationState.CLOSED:
            return await self.get_examination(ex_id=ex_id)
        sql = """
        UPDATE v3_examinations
            SET state=$2,
                updated_at=current_timestamp
        WHERE id=$1;
        """

        logger.debug(sql)

        try:
            await self.conn.execute(sql, ex_id, put_state.state)
            return await self.get_examination(ex_id=ex_id)
        except UniqueViolationError as e:
            logger.debug(e)
            raise HTTPException(
                409, "Cannot open examination. Only one OPEN examinaiton per case / app pair allowed"
            ) from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

    async def get_state(self, ex_id: UUID4) -> str:
        examination = await self.get_examination(ex_id)
        if examination is None:
            raise HTTPException(404, "The resource was not found")
        return examination.state

    async def _set_updated_time(self, ex_id: UUID4) -> bool:
        sql = """
        UPDATE v3_examinations
            SET updated_at=current_timestamp
        WHERE id=$1;
        """

        logger.debug(sql)

        try:
            await self.conn.execute(sql, ex_id)
            return True
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(400, "Examination could not be updated") from e

    async def add_job(self, ex_id: UUID4, job_id: Id) -> Tuple[Examination, bool]:
        if await self.get_state(ex_id) == ExaminationState.CLOSED:
            raise HTTPException(423, "Examination has state 'CLOSED': job can not be added!")

        sql = """
        INSERT INTO v3_examinations_jobs(ex_id, job_id)
        VALUES($1, $2);
        """

        logger.debug(sql)

        try:
            await self.conn.execute(sql, ex_id, job_id)
            await self._set_updated_time(ex_id)
            job_added = True
        except UniqueViolationError:
            job_added = False
        except PostgresError as e:
            raise HTTPException(500, f"Job could not be added: {e}") from e

        return await self.get_examination(ex_id=ex_id), job_added

    async def delete_job(self, ex_id: UUID4, job_id: Id):
        if await self.get_state(ex_id) == ExaminationState.CLOSED:
            raise HTTPException(423, "Examination has state 'CLOSED': job can not be deleted!")

        sql = """
        DELETE FROM v3_examinations_jobs
        WHERE ex_id = $1 AND job_id = $2
        RETURNING job_id;
        """

        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, ex_id, job_id)
            await self._set_updated_time(ex_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e

        if row is None:
            raise HTTPException(404, f"No job with id '{job_id}' found in examination")
        return await self.get_examination(ex_id)
