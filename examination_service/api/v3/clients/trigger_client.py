from asyncpg.exceptions import PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ....models.v3.examination import PostPreprocessingTrigger, PreprocessingTrigger, PreprocessingTriggerList
from ....singletons import logger


class PreprocessingTriggerClient:
    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _sql_mapping_as_json_insert():
        return """json_build_object(
            'id', id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'portal_app_id', portal_app_id,
            'tissue', tissue,
            'stain', stain
        )"""

    @staticmethod
    def _sql_mapping_as_json_select():
        return """json_build_object(
            'id', t.id,
            'creator_id', t.creator_id,
            'creator_type', t.creator_type,
            'portal_app_id', t.portal_app_id,
            'tissue', t.tissue,
            'stain', t.stain
        )"""

    async def add_preprocessing_trigger(self, trigger: PostPreprocessingTrigger) -> PreprocessingTrigger:
        sql = f"""
        INSERT INTO v3_preprocessing_triggers(
            creator_id,
            creator_type,
            portal_app_id,
            tissue,
            stain
        )
        VALUES(
            $1,
            $2,
            $3,
            $4,
            $5
        ) RETURNING {PreprocessingTriggerClient._sql_mapping_as_json_insert()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(
                sql, trigger.creator_id, trigger.creator_type, trigger.portal_app_id, trigger.tissue, trigger.stain
            )
        except UniqueViolationError as e:
            logger.debug(e)
            raise HTTPException(409, "Trigger already configured") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"{e}") from e

        if row is None:
            raise HTTPException(500, "Trigger could not be created")

        return PreprocessingTrigger.model_validate(row["json_build_object"])

    async def get_preprocessing_triggers(self) -> PreprocessingTriggerList:
        sql = f"""
        SELECT
        {self._sql_mapping_as_json_select()}
        FROM v3_preprocessing_triggers as t;
        """
        logger.debug(sql)

        try:
            results = await self.conn.fetch(sql)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"{e}") from e

        if results is None:
            raise HTTPException(500, "Triggers could not be fetched")

        item_count = len(results)
        items = [PreprocessingTrigger.model_validate(record["json_build_object"]) for record in results]
        return PreprocessingTriggerList(item_count=item_count, items=items)

    async def get_preprocessing_trigger(self, trigger_id: UUID4) -> PreprocessingTrigger:
        sql = f"""
        SELECT
        {PreprocessingTriggerClient._sql_mapping_as_json_select()}
        FROM v3_preprocessing_triggers as t
        WHERE t.id = $1;
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, trigger_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"{e}") from e

        if not row:
            raise HTTPException(404, f"No trigger with id {trigger_id} found")
        return PreprocessingTrigger.model_validate(row["json_build_object"])

    async def delete_preprocessing_trigger(self, trigger_id: UUID4):
        sql = """
        WITH deleted AS (
            DELETE FROM
                v3_preprocessing_triggers
            WHERE
                id = $1 RETURNING *
        )
        SELECT
            COUNT(*)
        FROM
            deleted;
        """
        logger.debug(sql)

        try:
            delete_count = await self.conn.fetchval(sql, trigger_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"{e}") from e

        if delete_count == 0:
            raise HTTPException(404, "The trigger does not exist")
