from typing import List

from asyncpg.exceptions import PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException

from ....models.v3.examination import (
    AppJob,
    PostPreprocessingRequest,
    PreprocessingRequest,
    PreprocessingRequestList,
    PreprocessingRequestQuery,
)
from ....singletons import logger

_APP_JOB_TO_JSON = """
json_build_object(
    'job_id', job_id,
    'app_id', app_id,
    'trigger_id', trigger_id
)
"""


class PreprocessingRequestClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_preprocessing_request(self, request: PostPreprocessingRequest) -> PreprocessingRequest:
        async with self.conn.transaction():
            sql = """
            INSERT INTO v3_preprocessing_requests (creator_id, creator_type, slide_id, created_at, updated_at, state)
            VALUES ($1, $2, $3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'OPEN')
            RETURNING id
            """
            logger.debug(sql)

            try:
                row = await self.conn.fetchrow(sql, request.creator_id, request.creator_type, request.slide_id)
                request_id = row["id"]
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, f"{e}") from e

            return await self.get_preprocessing_request(request_id)

    async def get_preprocessing_request(self, request_id) -> PreprocessingRequest:
        sql = f"""
        WITH v3_request_app_jobs AS (
            SELECT job_id, app_id, trigger_id
            FROM v3_preprocessing_app_jobs
            WHERE v3_preprocessing_app_jobs.request_id = $1
        )
        SELECT json_build_object(
            'id', id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'slide_id', slide_id,
            'created_at', EXTRACT(EPOCH FROM created_at)::int,
            'updated_at', EXTRACT(EPOCH FROM updated_at)::int,
            'app_jobs', (
                SELECT coalesce(
                    json_agg({_APP_JOB_TO_JSON}), '[]'::json
                )
                FROM v3_request_app_jobs
            ),
            'state', state
        ) v3_preprocessing_request
        FROM v3_preprocessing_requests
        WHERE id = $1
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, request_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"{e}") from e

        if not row:
            raise HTTPException(404, "No request found for given id")

        return PreprocessingRequest.model_validate(row["v3_preprocessing_request"])

    async def query_preprocessing_requests(self, query: PreprocessingRequestQuery) -> PreprocessingRequestList:
        if query.states:
            where_clause = "WHERE v3_preprocessing_requests.state = ANY($1)"
            query_data = [[state.value for state in query.states]]
        else:
            where_clause = ""
            query_data = []
        sql = f"""
        WITH v3_request_app_jobs AS (
            SELECT
                request_id,
                json_agg({_APP_JOB_TO_JSON}) agg_app_jobs
            FROM v3_preprocessing_app_jobs
            GROUP BY 1
        )
        SELECT
            json_agg(
                json_build_object(
                    'id', id,
                    'creator_id', creator_id,
                    'creator_type', creator_type,
                    'slide_id', slide_id,
                    'created_at', EXTRACT(EPOCH FROM created_at)::int,
                    'updated_at', EXTRACT(EPOCH FROM updated_at)::int,
                    'app_jobs', coalesce(agg_app_jobs, '[]'::json),
                    'state', state
                )
            ) agg_requests
        FROM (
            SELECT * FROM v3_preprocessing_requests {where_clause}
        ) requests
        LEFT JOIN v3_request_app_jobs ON v3_request_app_jobs.request_id = requests.id
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, *query_data)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"{e}") from e

        items = row["agg_requests"] if row else []
        if not items:
            return PreprocessingRequestList(items=[], item_count=0)
        return PreprocessingRequestList(items=items, item_count=len(items))

    async def process_preprocessing_request(self, request_id, app_jobs: List[AppJob]) -> PreprocessingRequest:
        async with self.conn.transaction():
            sql = """
            UPDATE v3_preprocessing_requests
            SET state = 'PROCESSED',
                updated_at = CURRENT_TIMESTAMP
            WHERE id = $1 AND state = 'OPEN'
            RETURNING id;
            """
            logger.debug(sql)

            try:
                row = await self.conn.fetchrow(sql, request_id)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, f"{e}") from e

            if not row:
                sql = """
                SELECT EXISTS(
                    SELECT 1 FROM v3_preprocessing_requests
                    WHERE id = $1 AND state = 'PROCESSED'
                );
                """
                exists = await self.conn.fetchval(sql, request_id)
                if exists:
                    raise HTTPException(409, "Request already processed")
                raise HTTPException(404, "Request not found")

            sql = """
            INSERT INTO v3_preprocessing_app_jobs (job_id, app_id, trigger_id, request_id)
                VALUES ($1, $2, $3, $4);
            """
            app_jobs_values = [(aj.job_id, aj.app_id, aj.trigger_id, request_id) for aj in app_jobs]
            try:
                await self.conn.executemany(sql, app_jobs_values)
            except UniqueViolationError as e:
                raise HTTPException(409, "App job must be unique within request") from e
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, f"{e}") from e

        return await self.get_preprocessing_request(request_id)
