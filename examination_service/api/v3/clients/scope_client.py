from http import HTTPStatus

from asyncpg.exceptions import DataError, PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ....models.v3.commons import Id
from ....models.v3.examination import Scope, ScopeList, ScopeQuery
from ....singletons import logger
from .examination_client import ExaminationClient
from .utils import concat_sql_statemment


class ScopeClient:
    def __init__(self, conn):
        self.conn = conn
        self.examination_client = ExaminationClient(conn)

    @staticmethod
    def _sql_scope_as_json_insert():
        return """json_build_object(
            'id', id,
            'examination_id', examination_id,
            'user_id', user_id,
            'created_at', EXTRACT(epoch FROM created_at)::int
        )"""

    @staticmethod
    def _sql_scope_as_json_select():
        return """json_build_object(
            'id', s.id,
            'examination_id', s.examination_id,
            'user_id', s.user_id,
            'created_at', EXTRACT(epoch FROM s.created_at)::int
        )"""

    @staticmethod
    def _sql_scope_with_app_as_json_select():
        return """
        json_build_object(
            'id', s.id,
            'examination_id', s.examination_id,
            'user_id', s.user_id,
            'created_at', EXTRACT(epoch FROM s.created_at)::INT,
            'app_id', s.app_id
        )
        """

    async def _validate_create_scope_request(self, examination_id: Id):
        raw_examination = await self.examination_client.get_raw_examination(examination_id)
        if raw_examination is None:
            raise HTTPException(
                HTTPStatus.NOT_FOUND, f"Cannot create scope, because no examination with id {examination_id} exists"
            )

    async def get_scopes(self, query: ScopeQuery = ScopeQuery(), skip: int = None, limit: int = None) -> ScopeList:
        where_terms = []
        args = []
        var_counter = 1

        if query.scopes is not None:
            where_terms.append(f"id = ANY(${var_counter}::uuid[])")
            args.append(query.scopes)
            var_counter += 1
        if query.examinations is not None:
            where_terms.append(f"examination_id = ANY(${var_counter})")
            args.append(query.examinations)
            var_counter += 1
        args.extend([limit, skip])

        sql = f"""
        WITH scopes AS (
            SELECT
                *
            FROM
                v3_scopes
            {await concat_sql_statemment(where_terms, prefix="WHERE ", separator=" AND ")}
            ORDER BY
                created_at DESC
        )
        SELECT
            count(scopes.*) AS count,
            (
                SELECT
                    json_agg(
                        {ScopeClient._sql_scope_with_app_as_json_select()}
                    ) AS items
                FROM
                    (
                        SELECT
                            scopes.*,
                            e.app_id
                        FROM
                            scopes
                            LEFT JOIN v3_examinations AS e
                                ON scopes.examination_id::uuid = e.id
                        LIMIT ${var_counter}
                        OFFSET ${var_counter + 1}
                    ) AS s
            )
        FROM
            scopes;
        """

        logger.debug(sql)

        try:
            raw_scopes = await self.conn.fetchrow(sql, *args)
        except DataError as e:
            raise HTTPException(422, "ERROR: No valid UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.args) from e

        if raw_scopes is None or len(raw_scopes) == 0:
            return ScopeList(item_count=0, items=[])

        return ScopeList.model_validate(
            {"item_count": raw_scopes["count"], "items": raw_scopes["items"] if raw_scopes["items"] else []}
        )

    async def get_scope_by_id(self, scope_id: Id) -> Scope:
        sql = f"""
        SELECT
        {self._sql_scope_as_json_select()}
        FROM v3_scopes as s
        WHERE s.id = $1;
        """
        logger.debug(sql)
        try:
            raw_scope = await self.conn.fetchrow(sql, scope_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        if raw_scope is None:
            raise HTTPException(HTTPStatus.NOT_FOUND, f"Scope with id {scope_id} not found")
        return await self.build_scope_with_app_id(raw_scope["json_build_object"])

    async def _query_raw_scope(self, sql: str, examination_id: Id, user_id: Id) -> dict:
        row = await self.conn.fetchrow(sql, examination_id, user_id)
        raw_scope = None if row is None else row["json_build_object"]
        return raw_scope

    async def get_scope(self, examination_id: Id, user_id: Id) -> Scope:
        sql = f"""
        SELECT
        {self._sql_scope_as_json_select()}
        FROM v3_scopes as s
        WHERE s.examination_id = $1 AND s.user_id = $2;
        """
        logger.debug(sql)
        try:
            raw_scope = await self._query_raw_scope(sql, examination_id, user_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        if raw_scope is None:
            return None
        else:
            return await self.build_scope_with_app_id(raw_scope)

    async def _insert_scope(self, examination_id: Id, user_id: Id):
        sql = f"""
        INSERT INTO v3_scopes(examination_id, user_id, created_at)
        VALUES(
            $1,
            $2,
            current_timestamp
        )
        ON CONFLICT (examination_id, user_id) DO NOTHING
        RETURNING {self._sql_scope_as_json_insert()};
        """
        logger.debug(sql)
        return await self._query_raw_scope(sql, examination_id, user_id)

    async def _add_scope(self, examination_id: Id, user_id: Id) -> dict:
        try:
            raw_scope = await self._insert_scope(examination_id, user_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        if raw_scope is None:
            raise HTTPException(
                HTTPStatus.BAD_REQUEST,
                f"Scope for examination {examination_id}, and user {user_id} was already created.",
            )
        return raw_scope

    async def create_scope(self, examination_id: UUID4, user_id: Id) -> Scope:
        await self._validate_create_scope_request(str(examination_id))
        raw_scope = await self._add_scope(str(examination_id), user_id)
        return await self.build_scope_with_app_id(raw_scope)

    async def build_scope_with_app_id(self, raw_scope):
        raw_ex = await self.examination_client.get_raw_examination(raw_scope["examination_id"])
        scope = Scope(
            id=raw_scope["id"],
            app_id=raw_ex["app_id"],
            created_at=raw_scope["created_at"],
            examination_id=raw_ex["id"],
            user_id=raw_scope["user_id"],
        )
        return scope
