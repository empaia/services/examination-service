from http import HTTPStatus

from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException

from ....models.v3.commons import Id
from ....models.v3.examination import AppUiStorage
from ....singletons import logger


class ScopeAppUiStorageClient:
    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _sql_scope_app_storage_as_json_insert():
        return """json_build_object(
            'scope_id', scope_id,
            'content', content,
            'created_at', EXTRACT(epoch FROM created_at)::int,
            'updated_at', EXTRACT(epoch FROM updated_at)::int
        )"""

    @staticmethod
    def _sql_scope_app_storage_as_json_select():
        return """json_build_object(
            'scope_id', ss.scope_id,
            'content', ss.content,
            'created_at', EXTRACT(epoch FROM ss.created_at)::int,
            'updated_at', EXTRACT(epoch FROM ss.updated_at)::int
        )"""

    @staticmethod
    def _sql_upsert_scope_app_storage():
        return f"""
        INSERT INTO v3_scope_app_storage(
            scope_id,
            content,
            created_at,
            updated_at
        )
        VALUES(
            $1,
            $2,
            current_timestamp,
            current_timestamp
        )
        ON CONFLICT(scope_id) DO UPDATE SET content = $2, updated_at = current_timestamp
        RETURNING {ScopeAppUiStorageClient._sql_scope_app_storage_as_json_insert()};
        """

    async def get_scope_app_storage(self, scope_id: Id) -> dict:
        sql = f"""
        SELECT
        {self._sql_scope_app_storage_as_json_select()}
        FROM v3_scope_app_storage as ss
        WHERE ss.scope_id = $1;
        """
        logger.debug(sql)
        try:
            row = await self.conn.fetchrow(sql, scope_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        if row is None:
            return AppUiStorage(content={})
        return AppUiStorage(content=row["json_build_object"]["content"])

    async def put_scope_app_storage(self, scope_id: Id, state: AppUiStorage) -> AppUiStorage:
        sql = self._sql_upsert_scope_app_storage()
        logger.debug(sql)
        try:
            row = await self.conn.fetchrow(
                sql,
                scope_id,
                state.content,
            )
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e
        return AppUiStorage(content=row["json_build_object"]["content"])
