from http import HTTPStatus

from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException

from ....models.v3.commons import Id
from ....models.v3.examination import AppUiStorage
from ....singletons import logger


class UserAppUiStorageClient:
    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _sql_user_app_storage_as_json_insert():
        return """json_build_object(
            'app_id', app_id,
            'user_id', user_id,
            'content', content,
            'created_at', EXTRACT(epoch FROM created_at)::int,
            'updated_at', EXTRACT(epoch FROM updated_at)::int
        )"""

    @staticmethod
    def _sql_user_app_storage_as_json_select():
        return """json_build_object(
            'app_id', us.app_id,
            'user_id', us.user_id,
            'content', us.content,
            'created_at', EXTRACT(epoch FROM us.created_at)::int,
            'updated_at', EXTRACT(epoch FROM us.updated_at)::int
        )"""

    @staticmethod
    def _sql_upsert_user_app_storage():
        return f"""
        INSERT INTO v3_user_app_storage(
            app_id,
            user_id,
            content,
            created_at,
            updated_at
        )
        VALUES(
            $1,
            $2,
            $3,
            current_timestamp,
            current_timestamp
        )
        ON CONFLICT(app_id, user_id) DO UPDATE SET content = $3, updated_at = current_timestamp
        RETURNING {UserAppUiStorageClient._sql_user_app_storage_as_json_insert()};
        """

    async def get_user_app_storage(self, app_id: Id, user_id: Id) -> dict:
        sql = f"""
        SELECT
        {self._sql_user_app_storage_as_json_select()}
        FROM v3_user_app_storage as us
        WHERE us.app_id = $1
        AND us.user_id = $2;
        """
        logger.debug(sql)
        try:
            row = await self.conn.fetchrow(sql, app_id, user_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(HTTPStatus.INTERNAL_SERVER_ERROR, e.with_traceback) from e
        if row is None:
            return AppUiStorage(content={})
        return AppUiStorage(content=row["json_build_object"]["content"])

    async def put_user_app_storage(self, app_id: Id, user_id: Id, state: AppUiStorage) -> AppUiStorage:
        sql = self._sql_upsert_user_app_storage()
        logger.debug(sql)
        try:
            row = await self.conn.fetchrow(
                sql,
                app_id,
                user_id,
                state.content,
            )
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, e.with_traceback) from e
        return AppUiStorage(content=row["json_build_object"]["content"])
