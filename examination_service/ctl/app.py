import asyncio
import logging
import sys

import typer

from ..models.utils.access_token_tools import AccessTokenTools
from ..settings import Settings
from .drop_db import run_drop_db
from .migrate_db import run_migrate_db

logger = logging.getLogger("esctl")
logger.addHandler(logging.StreamHandler(sys.stdout))

app = typer.Typer()


@app.command()
def migrate_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_migrate_db())


@app.command()
def drop_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_drop_db())


@app.command()
def create_access_token_tool_keys():
    # The constructor creates the public/private keys, if they do not exist:
    settings = Settings()

    _rsa_keys_directory = f"{settings.rsa_keys_directory}/v1"
    keys_creator = AccessTokenTools(_rsa_keys_directory)
    keys_creator.create_rsa_key_files(logger)

    _rsa_keys_directory = f"{settings.rsa_keys_directory}/v3"
    keys_creator = AccessTokenTools(_rsa_keys_directory)
    keys_creator.create_rsa_key_files(logger)
