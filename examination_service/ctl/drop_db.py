from asyncpg.exceptions import UndefinedTableError

from .commons import connect_db

TABLES = [
    "migration_steps",
    "es_migration_steps",
    "examinations",
    "examination_jobs",
    "scopes",
    "v3_examinations",
    "v3_examinations_jobs",
    "v3_scopes",
    "v3_preprocessing_triggers",
    "v3_preprocessing_requests",
    "v3_preprocessing_app_jobs",
]


async def run_drop_db():
    conn = await connect_db()

    for table in TABLES:
        try:
            await conn.execute(f"DROP TABLE {table};")
        except UndefinedTableError as e:
            print(e)
