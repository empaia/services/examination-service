async def step_03(conn):
    # Add indexes
    sql = """
    CREATE INDEX examinations_cr_id ON examinations USING btree (creator_id);
    CREATE INDEX examinations_cr_t ON examinations USING btree (creator_type);
    CREATE INDEX examinations_ca_id ON examinations USING btree (case_id);

    CREATE INDEX examinations_apps_ex ON examinations_apps USING btree (ex_id);
    CREATE INDEX examinations_apps_app ON examinations_apps USING btree (app_id);

    CREATE INDEX examinations_jobs_ex ON examinations_jobs USING btree (ex_id);
    CREATE INDEX examinations_jobs_app ON examinations_jobs USING btree (app_id);
    CREATE INDEX examinations_jobs_job ON examinations_jobs USING btree (job_id);
    """
    await conn.execute(sql)
