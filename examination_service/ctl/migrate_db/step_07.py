async def step_07(conn):
    sql = """
    CREATE TABLE v3_user_app_storage (
        app_id text NOT NULL,
        user_id text NOT NULL,
        content jsonb NOT NULL DEFAULT '{}'::jsonb,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL,
        PRIMARY KEY(app_id, user_id)
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE v3_scope_app_storage (
        scope_id uuid REFERENCES v3_scopes(id) ON DELETE CASCADE PRIMARY KEY,
        content jsonb NOT NULL DEFAULT '{}'::jsonb,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL
    );
    """
    await conn.execute(sql)
