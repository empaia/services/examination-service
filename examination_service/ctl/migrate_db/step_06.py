async def step_06(conn):
    sql = """
    CREATE TABLE v3_examinations (
        id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
        app_id text NOT NULL,
        case_id text NOT NULL,
        creator_id text NOT NULL,
        creator_type text NOT NULL,
        state text NOT NULL,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL
    );
    CREATE UNIQUE INDEX v3_examinations_open_state_app_id ON v3_examinations (app_id, case_id) WHERE state = 'OPEN';
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE v3_examinations_jobs (
        ex_id uuid REFERENCES v3_examinations(id) ON DELETE CASCADE,
        job_id text NOT NULL,
        PRIMARY KEY (ex_id, job_id)
    );
    """
    await conn.execute(sql)

    # Add indexes
    sql = """
    CREATE INDEX v3_examinations_cr_id ON v3_examinations USING btree (creator_id);
    CREATE INDEX v3_examinations_cr_t ON v3_examinations USING btree (creator_type);
    CREATE INDEX v3_examinations_ca_id ON v3_examinations USING btree (case_id);
    CREATE INDEX v3_examinations_app_id ON v3_examinations USING btree (app_id);
    CREATE INDEX v3_examinations_jobs_ex ON v3_examinations_jobs USING btree (ex_id);
    CREATE INDEX v3_examinations_jobs_job ON v3_examinations_jobs USING btree (job_id);
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE v3_scopes (
        id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
        examination_id text NOT NULL,
        user_id text NOT NULL,
        created_at timestamp NOT NULL
    );
    CREATE UNIQUE INDEX v3_eau ON v3_scopes (examination_id, user_id);
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE v3_preprocessing_triggers (
        id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
        creator_id text NOT NULL,
        creator_type text NOT NULL,
        portal_app_id text NOT NULL,
        tissue text NOT NULL,
        stain text NOT NULL
    );
    CREATE UNIQUE INDEX v3_pp_triggers_pts ON v3_preprocessing_triggers (portal_app_id, tissue, stain);
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE v3_preprocessing_requests (
        id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
        creator_id text NOT NULL,
        creator_type text NOT NULL,
        slide_id text NOT NULL,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL,
        state text NOT NULL
    );
    CREATE TABLE v3_preprocessing_app_jobs (
        job_id uuid PRIMARY KEY NOT NULL,
        app_id uuid NOT NULL,
        trigger_id uuid NOT NULL,
        request_id uuid REFERENCES v3_preprocessing_requests(id) NOT NULL
    );

    CREATE INDEX v3_pp_requests_states ON v3_preprocessing_requests (state);
    CREATE INDEX v3_pp_app_jobs_requests ON v3_preprocessing_app_jobs (request_id);
    """
    await conn.execute(sql)
