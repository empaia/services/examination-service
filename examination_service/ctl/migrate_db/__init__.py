import asyncio

from asyncpg.exceptions import CannotConnectNowError

from ..commons import connect_db
from .step_01 import step_01
from .step_02 import step_02
from .step_03 import step_03
from .step_04 import step_04
from .step_05 import step_05
from .step_06 import step_06
from .step_07 import step_07

MIGRATION_STEPS = [step_01, step_02, step_03, step_04, step_05, step_06, step_07]
TABLE_NAME_PREFIX = "es"


async def _run_migrate_db(target_step=None):
    conn = await connect_db()

    async with conn.transaction():
        step_id = 0

        # existence of migration_steps table is only checked for existing installations
        # new installations use table name prefix
        table_name = "migration_steps"

        sql_template = """
        SELECT EXISTS (
            SELECT FROM information_schema.tables
            WHERE table_schema = 'public' AND table_name = '{table_name}'
        );
        """

        sql = sql_template.format(table_name=table_name)
        row = await conn.fetchrow(sql)

        if not row["exists"]:
            table_name = f"{TABLE_NAME_PREFIX}_migration_steps"

        print("MIGRATION STEPS TABLE NAME:", table_name)

        sql = sql_template.format(table_name=table_name)
        row = await conn.fetchrow(sql)

        if not row["exists"]:
            sql = f"""
            CREATE TABLE {table_name} (
                id SERIAL PRIMARY KEY,
                timestamp TIMESTAMP DEFAULT current_timestamp
            );
            """
            await conn.execute(sql)
        else:
            sql = f"""
            SELECT id FROM {table_name} ORDER BY id DESC
            """
            row = await conn.fetchrow(sql)

            if row is not None:
                step_id = row["id"]

        remaining_steps = MIGRATION_STEPS[step_id:]
        if target_step is not None:
            remaining_steps = MIGRATION_STEPS[step_id:target_step]

        for i, step in enumerate(remaining_steps):
            print(f"DATABASE MIGRATION STEP {step_id + i + 1}")
            await step(conn=conn)
            sql = f"INSERT INTO {table_name} DEFAULT VALUES returning id;"
            await conn.execute(sql)


async def run_migrate_db(target_step=None, trial_interval=1, trials=30):
    print("START DATABASE MIGRATIONS")

    migration_completed = False

    for i in range(trials):
        try:
            await _run_migrate_db(target_step=target_step)
            migration_completed = True
            break
        except (ConnectionRefusedError, CannotConnectNowError):
            print(
                f"WARNING: ConnectionRefusedError during migration trial {i+1}/{trials}, "
                f"trying again in {trial_interval} seconds..."
            )

        await asyncio.sleep(trial_interval)

    if not migration_completed:
        raise RuntimeError("ERROR: Could not connect to DB for migration")

    print("END DATABASE MIGRATIONS")
