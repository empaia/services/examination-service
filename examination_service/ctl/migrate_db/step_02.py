async def step_02(conn):
    sql = """
    CREATE TABLE examinations_apps (
        ex_id uuid REFERENCES examinations(id) ON DELETE CASCADE,
        app_id text NOT NULL,
        PRIMARY KEY (ex_id, app_id)
    );
    """
    await conn.execute(sql)

    sql = """
    ALTER TABLE examinations_jobs ADD COLUMN IF NOT EXISTS app_id text NOT NULL;
    ALTER TABLE examinations_jobs DROP CONSTRAINT examinations_jobs_pkey;
    ALTER TABLE examinations_jobs ADD PRIMARY KEY (ex_id, app_id, job_id);
    """
    await conn.execute(sql)
