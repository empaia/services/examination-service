async def step_04(conn):
    sql = """
    ALTER TABLE examinations DROP COLUMN IF EXISTS state_changed_at;
    ALTER TABLE examinations DROP COLUMN IF EXISTS state_last_viewed_at;
    """
    await conn.execute(sql)
