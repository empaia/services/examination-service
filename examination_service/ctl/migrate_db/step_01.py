from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    # Add extension to create uuid values
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    sql = """
    CREATE TABLE examinations (
        id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
        case_id text NOT NULL,
        creator_id text NOT NULL,
        creator_type text NOT NULL,
        state text NOT NULL,
        state_changed_at timestamp NOT NULL,
        state_last_viewed_at timestamp NOT NULL,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE examinations_jobs (
        ex_id uuid REFERENCES examinations(id) ON DELETE CASCADE,
        job_id text NOT NULL,
        PRIMARY KEY (ex_id, job_id)
    );
    """
    await conn.execute(sql)
