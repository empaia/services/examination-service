async def step_05(conn):
    sql = """
    CREATE TABLE scopes (
        id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
        examination_id text NOT NULL,
        app_id text NOT NULL,
        user_id text NOT NULL,
        created_at timestamp NOT NULL
    );
    CREATE UNIQUE INDEX eau ON scopes (examination_id, app_id, user_id);
    """
    await conn.execute(sql)
