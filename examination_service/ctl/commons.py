import asyncpg

from ..db import set_type_codecs
from ..singletons import settings


async def connect_db() -> asyncpg.Connection:
    conn = await asyncpg.connect(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
    await set_type_codecs(conn=conn)
    return conn
