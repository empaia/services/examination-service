from contextlib import asynccontextmanager

import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api.root import add_routes_root
from .api.v1 import add_routes_v1
from .api.v3 import add_routes_v3
from .late_init import LateInit
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

late_init = LateInit()


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
    yield


app = FastAPI(
    debug=settings.debug,
    title="Examination Service API",
    version=version,
    description="API for EMPAIA Examination Service.",
    root_path=settings.root_path,
    openapi_url=openapi_url,
    lifespan=lifespan,
)

app_v1 = FastAPI(openapi_url=openapi_url)
app_v3 = FastAPI(openapi_url=openapi_url)

if settings.cors_allow_origins:
    for app_obj in [app_v1, app_v3]:
        app_obj.add_middleware(
            CORSMiddleware,
            allow_origins=settings.cors_allow_origins,
            allow_credentials=settings.cors_allow_credentials,
            allow_methods=["*"],
            allow_headers=["*"],
        )

add_routes_root(app, late_init)
add_routes_v1(app_v1, late_init)
add_routes_v3(app_v3, late_init)
app.mount("/v1", app_v1)
app.mount("/v3", app_v3)
