from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    db_host: str = "localhost"
    db_port: str = "5432"
    db_username: str = "admin"
    db_password: str = "secret"
    db: str = "es"
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    disable_openapi: bool = False
    debug: bool = False
    root_path: str = ""
    rsa_keys_directory: str = "./rsa/"
    scope_token_exp: int = 5 * 60  # access token expires after five minutes by default

    model_config = SettingsConfigDict(env_file=".env", env_prefix="ES_", extra="ignore")
