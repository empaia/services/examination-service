import logging

from .settings import Settings

settings = Settings()

rsa_keys_directory = settings.rsa_keys_directory.rstrip("/")

logger = logging.getLogger("uvicorn.es")

if settings.debug:
    logger.setLevel(logging.DEBUG)
