FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.4.2@sha256:5c1a951baeed230dc36a9d36c3689faf7e639a3a2271eb4e64cc7660062d6f85 AS builder
COPY . /examination_service
WORKDIR /examination_service
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.3.1@sha256:54c07b7d87e7d70248fa590f4ed7e0217e2e11b76bb569724807acc82488197b
COPY --from=builder /examination_service/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /examination_service/dist /artifacts
RUN pip install /artifacts/*.whl
COPY ./run.sh /opt/app/bin/run.sh
WORKDIR /opt/app/bin/
