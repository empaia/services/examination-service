#!/usr/bin/env bash

esctl create-access-token-tool-keys && esctl migrate-db && uvicorn examination_service.app:app $@
