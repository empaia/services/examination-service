import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_add_app_get():
    # POST examination
    payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
    post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
    assert post_response.status_code == 201
    ex_id = post_response.json()["id"]
    updated_at = post_response.json()["updated_at"]

    for i in range(2):
        # POST App
        time.sleep(2)
        app_id = str(uuid4())
        post_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
        assert post_response.status_code == 200
        post_response = post_response.json()
        print(post_response)
        assert post_response["id"] == ex_id
        assert id_in_examination_app(post_response["apps"], app_id)
        assert updated_at < post_response["updated_at"]
        # GET with jobs and apps, Test POST app
        get_response = requests.get(f"{es_url}/v1/examinations/{ex_id}").json()
        assert len(get_response["apps"]) == i + 1
        assert id_in_examination_app(post_response["apps"], app_id)


def id_in_examination_app(apps, app_id):
    for entry in apps:
        if entry["id"] == app_id:
            return True
    return False
