from uuid import uuid4

import requests

from ..singletons import es_url


def test_post_multiple_put_query_case_id():
    case_id = str(uuid4())
    for i in range(10):
        # POST
        payload = {
            "case_id": case_id if i % 2 == 0 else str(uuid4()),
            "creator_id": str(uuid4()),
            "creator_type": "USER",
        }
        post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
        print(post_response.text)
        assert post_response.status_code == 201
        # we need to close the examinations as there must only be one open examination per case
        requests.put(f"{es_url}/v2/examinations/{post_response.json()['id']}/state", json={"state": "CLOSED"})

    # Test PUT query
    query = {"cases": [case_id]}
    put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
    assert put_response["item_count"] == 5
    assert len(put_response["items"]) == 5


def test_post_multiple_put_query_creator_id():
    creator_id = str(uuid4())
    for i in range(10):
        # POST
        payload = {
            "case_id": str(uuid4()),
            "creator_id": creator_id if i % 2 == 0 else str(uuid4()),
            "creator_type": "USER",
        }
        post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
        assert post_response.status_code == 201

    # Test PUT query
    query = {"creators": [creator_id]}
    put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
    assert put_response["item_count"] == 5
    assert len(put_response["items"]) == 5


def test_post_multiple_put_query_creator_id_no_results():
    creator_id = str(uuid4())
    # Test PUT query
    query = {"creators": [creator_id]}
    put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
    assert put_response["item_count"] == 0
    assert len(put_response["items"]) == 0


# tests are obsolete at the moment because there is only one entity of creator type (USER)
# def test_post_multiple_put_query_creator_type():
#     for i in range(10):
#         # POST
#         payload = {
#             "case_id": str(uuid4()),
#             "creator_id": str(uuid4()),
#             "creator_type": "USER" if i % 2 == 0 else "AUTOMATIC",
#         }
#         print(payload)
#         post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
#         assert post_response.status_code == 200
#     # Test PUT query
#     query = {"creator_types": ["USER"]}
#     put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
#     assert put_response["item_count"] == 5
#     assert len(put_response["items"]) == 5
#     query = {"creator_types": ["AUTOMATIC"]}
#     put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
#     assert put_response["item_count"] == 5
#     assert len(put_response["items"]) == 5
#     query = {"creator_types": ["USER", "AUTOMATIC"]}
#     put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
#     assert put_response["item_count"] == 10
#     assert len(put_response["items"]) == 10


# def test_post_multiple_put_query_creator_type_and_creator_id():
#     creator_id = str(uuid4())
#     for i in range(10):
#         # POST
#         payload = {
#             "case_id": str(uuid4()),
#             "creator_id": creator_id if i < 2 else str(uuid4()),
#             "creator_type": "USER" if i % 2 == 0 else "AUTOMATIC",
#             "state": "OPEN",
#         }
#         post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
#         assert post_response.status_code == 200
#     # Test PUT query
#     query = {"creator_types": ["USER"], "creators": [creator_id]}
#     put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
#     assert put_response["item_count"] == 1
#     assert len(put_response["items"]) == 1
#     query = {"creator_types": ["AUTOMATIC"], "creators": [creator_id]}
#     put_response = requests.put(f"{es_url}/v1/examinations/query", json=query).json()
#     assert put_response["item_count"] == 1
#     assert len(put_response["items"]) == 1


# def test_post_multiple_put_query_creator_type_and_creator_id_skip_limit():
#     creator_id = str(uuid4())
#     for i in range(100):
#         # POST
#         payload = {
#             "case_id": str(uuid4()),
#             "creator_id": creator_id,
#             "creator_type": "USER" if i % 2 == 0 else "AUTOMATIC",
#             "state": "OPEN",
#         }
#         post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
#         assert post_response.status_code == 200
#     # Test PUT query skip & limit
#     params = {"skip": 10, "limit": 20}
#     query = {"creator_types": ["USER"], "creators": [creator_id]}
#     put_response = requests.put(f"{es_url}/v1/examinations/query", json=query, params=params).json()
#     assert put_response["item_count"] == 20
#     assert len(put_response["items"]) == 20
