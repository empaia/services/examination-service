from uuid import uuid4

import requests

from examination_service.models.v1.examination import Examination

from ..singletons import es_url


def test_get_examination_by_job_id():
    # POST
    payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
    post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
    assert post_response.status_code == 201
    ex_id = post_response.json()["id"]

    # POST App
    app_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert put_response.status_code == 200

    get_response = requests.get(f"{es_url}/v1/jobs/{uuid4()}/examinations")
    assert get_response.status_code == 404

    for _ in range(5):
        # POST Job
        job_id = str(uuid4())
        put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add")
        assert put_response.status_code == 200

        # GET ex
        get_response = requests.get(f"{es_url}/v1/jobs/{job_id}/examinations")
        assert get_response.status_code == 200
        ex = get_response.json()
        Examination.model_validate(ex)
        assert ex["id"] == ex_id
