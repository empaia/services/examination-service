from uuid import uuid4

import requests

from ..singletons import es_url


def test_post_examination():
    # POST
    payload = {
        "case_id": str(uuid4()),
        "creator_id": str(uuid4()),
        "creator_type": "iNvaLId TyPe",
        "state": "iNvaLId TyPe",
    }
    post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
    assert post_response.status_code == 422
