from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_add_job_app_get():
    # POST examination
    payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
    post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
    assert post_response.status_code == 201
    ex_id = post_response.json()["id"]

    # POST App
    app_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert put_response.status_code == 200

    # POST Job twice
    job_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add")
    assert put_response.status_code == 200
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add")
    assert put_response.status_code == 405

    # POST to App that does not exist
    app_id = "not_exist"
    job_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add")
    assert put_response.status_code == 404
    assert put_response.json()["detail"] == f"App with ID {app_id} does not exist for given examination"

    # CLOSED
    # POST App
    app_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert put_response.status_code == 200

    # CLOSE ex
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200

    # POST Job
    job_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add")
    assert put_response.status_code == 423
