import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_add_app_get():
    # POST examination
    payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
    post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
    assert post_response.status_code == 201
    ex_id = post_response.json()["id"]

    # POST App
    app_id = str(uuid4())
    post_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert post_response.status_code == 200
    updated_at = post_response.json()["updated_at"]

    time.sleep(2)

    # POST App again
    post_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert post_response.status_code == 405

    # GET Ex
    get_response = requests.get(f"{es_url}/v1/examinations/{ex_id}")
    assert get_response.status_code == 200
    assert updated_at == get_response.json()["updated_at"]

    # CLOSE ex
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200

    # POST App
    app_id = str(uuid4())
    post_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert post_response.status_code == 423
