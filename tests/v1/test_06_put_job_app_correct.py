import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_add_job_app_get():
    # POST examination
    payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
    post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
    assert post_response.status_code == 201
    ex_id = post_response.json()["id"]

    # POST App
    app_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert put_response.status_code == 200
    put_response = put_response.json()
    assert put_response["id"] == ex_id
    assert put_response["apps"][0]["id"] == app_id
    updated_at = put_response["updated_at"]

    for i in range(3):
        # POST Job
        time.sleep(2)
        job_id = str(uuid4())
        put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add")
        print(put_response)
        assert put_response.status_code == 200
        put_response = put_response.json()
        assert put_response["id"] == ex_id
        assert put_response["apps"][0]["id"] == app_id
        assert job_id in put_response["apps"][0]["jobs"]
        assert updated_at < put_response["updated_at"]

        # GET job for examination and app
        get_response = requests.get(f"{es_url}/v1/examinations/{ex_id}").json()
        assert len(get_response["apps"][0]["jobs"]) == i + 1

    get_response = requests.get(f"{es_url}/v1/examinations/{ex_id}", json=payload).json()
    print(get_response)
    assert len(get_response["apps"][0]["jobs"]) == 3
    assert len(get_response["apps"]) == 1
