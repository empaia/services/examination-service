from uuid import uuid4

from .helpers import Assert, ExaminationHelpers


def test_get_or_create_scope():
    user_id = str(uuid4())
    case_id = str(uuid4())
    app_id = str(uuid4())

    # Try to create scope with invalid examination id
    invalid_examination_id = "ex_id"
    ExaminationHelpers.create_scope(invalid_examination_id, app_id, user_id, expected_status_code=422)

    # Try to create scope with invalid examination id
    invalid_examination_id = str(uuid4())
    ExaminationHelpers.create_scope(invalid_examination_id, app_id, user_id, expected_status_code=400)

    # Create examination
    examination_data = ExaminationHelpers.create_examination(case_id, user_id)
    examination_id = examination_data["id"]

    # Try to create scope with unregistered app id
    ExaminationHelpers.create_scope(examination_id, app_id, user_id, expected_status_code=400)

    # Register app
    examination_data = ExaminationHelpers.register_app(examination_id, app_id)
    assert examination_data["id"] == examination_id

    # Create second examination
    ExaminationHelpers.close_examination(
        examination_data["id"]
    )  # close this one as there must not be two open examinations per case
    examination_data_2 = ExaminationHelpers.create_examination(case_id, user_id)
    examination_id_2 = examination_data_2["id"]

    # Register second app
    app_id_2 = str(uuid4())
    examination_data_2 = ExaminationHelpers.register_app(examination_id_2, app_id_2)
    assert examination_data_2["id"] == examination_id_2

    # Try to create scope with second app id that is not registered in first examination
    ExaminationHelpers.create_scope(examination_id, app_id_2, user_id, expected_status_code=400)

    # Try to create scope with first app id that is not registered in second examination
    ExaminationHelpers.create_scope(examination_id_2, app_id, user_id, expected_status_code=400)

    # Verify successful scope creation
    scope_response = ExaminationHelpers.create_scope(examination_id, app_id, user_id)
    assert "id" in scope_response
    assert scope_response["app_id"] == app_id
    assert scope_response["examination_id"] == examination_id
    assert scope_response["user_id"] == user_id
    assert "created_at" in scope_response
    scope_data = ExaminationHelpers.get_scope_data(scope_response["id"])
    scope_response_2 = ExaminationHelpers.create_scope(examination_id_2, app_id_2, user_id)
    assert "id" in scope_response_2
    assert scope_response_2["app_id"] == app_id_2
    assert scope_response_2["examination_id"] == examination_id_2
    assert scope_response_2["user_id"] == user_id
    assert "created_at" in scope_response_2
    scope_data_2 = ExaminationHelpers.get_scope_data(scope_response_2["id"])
    # Verify that no data mismatch occurred
    assert scope_response["id"] != scope_response_2["id"]
    assert scope_data["id"] == scope_response["id"]
    assert scope_data["app_id"] == app_id
    assert scope_data["examination_id"] == examination_id
    assert scope_data["user_id"] == user_id
    assert "created_at" in scope_data
    assert scope_data_2["id"] != scope_data["id"]
    assert scope_data_2["id"] == scope_response_2["id"]
    assert scope_data_2["app_id"] == app_id_2
    assert scope_data_2["examination_id"] == examination_id_2
    assert scope_data_2["user_id"] == user_id
    assert "created_at" in scope_data_2

    # Verify that the scope cannot be created a second time
    ExaminationHelpers.create_scope(examination_id, app_id, user_id, expected_status_code=400)

    # Verify successful scope access
    scope_response_again = ExaminationHelpers.get_scope(examination_id, app_id, user_id)
    assert scope_response["id"] == scope_response_again["id"]
    scope_data_again = ExaminationHelpers.get_scope_data(scope_response_again["id"])
    scope_response_again_2 = ExaminationHelpers.get_scope(examination_id_2, app_id_2, user_id)
    assert scope_response_2["id"] == scope_response_again_2["id"]
    scope_data_again_2 = ExaminationHelpers.get_scope_data(scope_response_again_2["id"])
    # Verify that the data is the same
    Assert.dicts_equal(scope_data, scope_data_again)
    Assert.dicts_equal(scope_data_2, scope_data_again_2)

    # Verify that another user gets a different scope
    user_id_2 = str(uuid4())
    assert user_id != user_id_2
    scope_response_user_2 = ExaminationHelpers.create_scope(examination_id, app_id, user_id_2)
    scope_data_user_2 = ExaminationHelpers.get_scope_data(scope_response_user_2["id"])
    scope_response_2_user_2 = ExaminationHelpers.create_scope(examination_id_2, app_id_2, user_id_2)
    scope_data_2_user_2 = ExaminationHelpers.get_scope_data(scope_response_2_user_2["id"])
    # Verify that no data mismatch occurred
    assert scope_data_user_2["id"] != scope_data["id"]
    assert scope_data_user_2["id"] != scope_data_2["id"]
    assert scope_data_user_2["app_id"] == app_id
    assert scope_data_user_2["examination_id"] == examination_id
    assert scope_data_user_2["user_id"] == user_id_2
    assert scope_data_2_user_2["id"] != scope_data_user_2["id"]
    assert scope_data_2_user_2["id"] != scope_data["id"]
    assert scope_data_2_user_2["id"] != scope_data_2["id"]
    assert scope_data_2_user_2["app_id"] == app_id_2
    assert scope_data_2_user_2["examination_id"] == examination_id_2
    assert scope_data_2_user_2["user_id"] == user_id_2
