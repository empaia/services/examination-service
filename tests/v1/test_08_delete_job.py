import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_get_examination_id_by_job_id():
    # POST
    payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
    post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
    assert post_response.status_code == 201
    ex_id = post_response.json()["id"]

    # POST App
    app_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/add")
    assert put_response.status_code == 200

    # POST Jobs
    job_id_1 = uuid4()
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id_1}/add")
    assert put_response.status_code == 200
    assert len(put_response.json()["apps"][0]["jobs"]) == 1
    job_id_2 = uuid4()
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id_2}/add")
    assert put_response.status_code == 200
    assert len(put_response.json()["apps"][0]["jobs"]) == 2
    updated_at = put_response.json()["updated_at"]

    # DELETE Job
    time.sleep(2)
    put_response = requests.delete(f"{es_url}/v1/examinations/{ex_id}/jobs/{job_id_1}")
    assert put_response.status_code == 200
    ex = put_response.json()
    assert len(ex["apps"][0]["jobs"]) == 1
    assert updated_at < ex["updated_at"]

    put_response = requests.delete(f"{es_url}/v1/examinations/{ex_id}/jobs/{job_id_1}")
    assert put_response.status_code == 404

    # CLOSE ex
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200

    # DELETE Job
    put_response = requests.delete(f"{es_url}/v1/examinations/{ex_id}/jobs/{job_id_2}")
    assert put_response.status_code == 423

    get_response = requests.get(f"{es_url}/v1/examinations/{ex_id}").json()
    assert len(get_response["apps"][0]["jobs"]) == 1
