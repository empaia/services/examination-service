import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_post_multiple_get_specific_examination():
    for _ in range(10):
        # POST
        payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
        post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
        assert post_response.status_code == 201
        ex_id = post_response.json()["id"]
        # Test GET by ID
        get_response = requests.get(f"{es_url}/v1/examinations/{ex_id}").json()
        assert get_response["case_id"] == payload["case_id"]
        assert get_response["creator_id"] == payload["creator_id"]
        assert get_response["creator_type"] == payload["creator_type"]
        assert get_response["state"] == "OPEN"


def test_post_multiple_get_all_examinations():
    start_count = requests.get(f"{es_url}/v1/examinations").json()["item_count"]
    for i in range(10):
        # POST
        payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
        post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
        assert post_response.status_code == 201
        # Test GET all
        get_response = requests.get(f"{es_url}/v1/examinations").json()
        assert get_response["item_count"] == i + 1 + start_count
        assert len(get_response["items"]) == i + 1 + start_count

    # GET all: check order
    ex_ids = []
    for i in range(5):
        # POST
        payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
        post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
        assert post_response.status_code == 201
        ex_ids.append(post_response.json()["id"])

    time.sleep(2)

    # Close last five ex
    for ex_id in ex_ids:
        update_payload = {"state": "CLOSED"}
        put_response = requests.put(f"{es_url}/v1/examinations/{ex_id}/state", json=update_payload)
        assert put_response.status_code == 200

    # Check order
    get_response = requests.get(f"{es_url}/v1/examinations").json()
    for i in range(5):
        assert ex_ids[4 - i] == get_response["items"][i]["id"]


def test_post_multiple_get_all_examinations_skip_limit():
    start_count = requests.get(f"{es_url}/v1/examinations").json()["item_count"]
    for i in range(10):
        # POST
        payload = {"case_id": str(uuid4()), "creator_id": str(uuid4()), "creator_type": "USER"}
        post_response = requests.post(f"{es_url}/v1/examinations", json=payload)
        assert post_response.status_code == 201
    # Test GET all skip & limit
    params = {"skip": 5, "limit": 2}
    get_response = requests.get(f"{es_url}/v1/examinations", params=params).json()
    assert get_response["item_count"] == i + 1 + start_count
    assert len(get_response["items"]) == 2
