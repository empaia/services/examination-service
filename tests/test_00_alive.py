import requests

from examination_service.models.commons import ServiceStatus

from .singletons import es_url


def test_alive():
    url = f"{es_url}/alive"
    r = requests.get(url)
    assert r.status_code == 200
    status = r.json()
    ServiceStatus.model_validate(status)
    assert r.json()["status"] == "ok"
    assert r.json()["version"]
