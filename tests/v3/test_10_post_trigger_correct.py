from uuid import uuid4

import requests

from ..singletons import es_url


def test_post_trigger():
    payload = {
        "portal_app_id": str(uuid4()),
        "tissue": "LUNG",
        "stain": "HE",
        "creator_id": str(uuid4()),
        "creator_type": "USER",
    }
    post_response = requests.post(f"{es_url}/v3/preprocessing-triggers", json=payload)
    assert post_response.status_code == 201
