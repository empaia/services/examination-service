from uuid import uuid4

import requests

from ..singletons import es_url
from .helpers import ExaminationHelpers


def _test_user_app_storage(user_id, examination_id, existing_storage=None):
    # Create scope
    scope_data = ExaminationHelpers.create_or_get_scope(examination_id, user_id, 201)
    assert "id" in scope_data
    assert scope_data["examination_id"] == examination_id
    assert scope_data["user_id"] == user_id
    scope_id = scope_data["id"]

    # GET storage
    get_response_1 = requests.get(f"{es_url}/v3/scopes/{scope_id}/app-ui-storage/user")
    assert get_response_1.status_code == 200
    get_response_1 = get_response_1.json()
    if existing_storage is None:
        assert get_response_1["content"] == {}
    else:
        for key in existing_storage["content"].keys():
            assert key in get_response_1["content"]
            assert existing_storage["content"][key] == get_response_1["content"][key]

    # GET storage bad scope_id
    bad_scope_id = str(uuid4())
    get_response_2 = requests.get(f"{es_url}/v3/scopes/{bad_scope_id}/app-ui-storage/user")
    assert get_response_2.status_code == 404

    # PUT storage bad scope_id
    payload = {
        "content": {
            "name": "max",
            "street": "milchstraße",
            "streetnr": 42,
            "cool?": True,
            "this_will_be_removed": 3.0,
        }
    }
    put_response_1 = requests.put(f"{es_url}/v3/scopes/{bad_scope_id}/app-ui-storage/user", json=payload)
    assert put_response_1.status_code == 404

    # PUT storage
    put_response_1 = requests.put(f"{es_url}/v3/scopes/{scope_id}/app-ui-storage/user", json=payload)
    assert put_response_1.status_code == 201
    put_response_1 = put_response_1.json()
    for key in payload["content"].keys():
        assert key in put_response_1["content"]
        assert payload["content"][key] == put_response_1["content"][key]

    # PUT (update) storage
    payload = {
        "content": {
            "name": "karen",
            "street": "milchstraße",
            "streetnr": 43,
            "cool?": False,
        }
    }
    put_response_2 = requests.put(f"{es_url}/v3/scopes/{scope_id}/app-ui-storage/user", json=payload)
    assert put_response_2.status_code == 201
    put_response_2 = put_response_2.json()
    for key in payload["content"].keys():
        assert key in put_response_2["content"]
        assert payload["content"][key] == put_response_2["content"][key]
    assert "this_will_be_removed" not in put_response_2["content"].keys()

    # GET storage
    get_response_3 = requests.get(f"{es_url}/v3/scopes/{scope_id}/app-ui-storage/user")
    assert get_response_3.status_code == 200
    get_response_3 = get_response_3.json()
    for key in payload["content"].keys():
        assert key in get_response_3["content"]
        assert payload["content"][key] == get_response_3["content"][key]

    return get_response_3


def test_get_or_create_user_storage():
    user_id_1 = str(uuid4())
    user_id_2 = str(uuid4())
    case_id = str(uuid4())
    app_id_1 = str(uuid4())
    app_id_2 = str(uuid4())

    for app_id in [app_id_1, app_id_2]:
        # Create examination
        examination_data = ExaminationHelpers.create_or_get_examination(case_id, user_id_1, app_id)
        examination_id_1 = examination_data["id"]

        # user_1
        storage_1 = _test_user_app_storage(user_id_1, examination_id_1, existing_storage=None)
        # other examination. user app ui storage should equal storage_1
        ExaminationHelpers.close_examination(examination_id_1)
        examination_data = ExaminationHelpers.create_or_get_examination(case_id, user_id_1, app_id)
        examination_id_2 = examination_data["id"]
        _ = _test_user_app_storage(user_id_1, examination_id_2, existing_storage=storage_1)
        # user_2
        # test user app storage for second user is empty (and not the storage from user_1)
        _ = _test_user_app_storage(user_id_2, examination_id_2, existing_storage=None)
