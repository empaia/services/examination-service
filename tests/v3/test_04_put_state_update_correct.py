import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_update_state_get():
    # put
    payload = {
        "case_id": str(uuid4()),
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": str(uuid4()),
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    ex_id = put_response.json()["id"]
    # GET
    first_get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}", json=payload).json()
    assert first_get_response["state"] == "OPEN"
    # PUT state
    time.sleep(2)
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200
    put_response = put_response.json()
    assert put_response["case_id"] == payload["case_id"]
    assert put_response["creator_id"] == payload["creator_id"]
    assert put_response["creator_type"] == payload["creator_type"]
    assert put_response["state"] == update_payload["state"]
    # GET, Test PUT
    get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}").json()
    assert get_response["state"] == update_payload["state"]
    assert get_response["updated_at"] > first_get_response["updated_at"]
