from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_examination():
    # PUT
    payload = {
        "case_id": str(uuid4()),
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": str(uuid4()),
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    put_response = put_response.json()
    assert put_response["case_id"] == payload["case_id"]
    assert put_response["creator_id"] == payload["creator_id"]
    assert put_response["creator_type"] == payload["creator_type"]
    assert put_response["app_id"] == payload["app_id"]
    ex_id = put_response["id"]
    # GET
    get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}")
    assert get_response.status_code == 200
    get_response = get_response.json()
    assert get_response["case_id"] == payload["case_id"]
    assert get_response["creator_id"] == payload["creator_id"]
    assert get_response["creator_type"] == payload["creator_type"]
    assert get_response["app_id"] == payload["app_id"]
    assert len(get_response["jobs"]) == len(put_response["jobs"])
    assert get_response["state"] == "OPEN"


def test_put_two_examination_same_case_different_app():
    case_id = str(uuid4())
    for _i in range(2):
        # PUT
        payload = {
            "case_id": case_id,
            "creator_id": str(uuid4()),
            "creator_type": "SERVICE",
            "app_id": str(uuid4()),
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201
        put_response = put_response.json()
        assert put_response["case_id"] == payload["case_id"]
        assert put_response["creator_id"] == payload["creator_id"]
        assert put_response["creator_type"] == payload["creator_type"]
        assert put_response["app_id"] == payload["app_id"]
        ex_id = put_response["id"]
        # GET
        get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}")
        assert get_response.status_code == 200
        get_response = get_response.json()
        assert get_response["case_id"] == payload["case_id"]
        assert get_response["creator_id"] == payload["creator_id"]
        assert get_response["creator_type"] == payload["creator_type"]
        assert get_response["app_id"] == payload["app_id"]
        assert len(get_response["jobs"]) == len(put_response["jobs"])
        assert get_response["state"] == "OPEN"


def test_put_two_examination_same_case_same_app():
    case_id = str(uuid4())
    app_id = str(uuid4())
    creator_ids = [str(uuid4()), str(uuid4())]
    for i in range(2):
        # PUT
        payload = {
            "case_id": case_id,
            "creator_id": creator_ids[i],
            "creator_type": "SERVICE",
            "app_id": app_id,
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        # new examination created
        if i == 0:
            assert put_response.status_code == 201
            put_response = put_response.json()
            assert put_response["creator_id"] == creator_ids[0]
        # no new examination, receive old examination
        elif i == 1:
            assert put_response.status_code == 200
            put_response = put_response.json()
            assert put_response["creator_id"] == creator_ids[0]
        assert put_response["case_id"] == payload["case_id"]
        assert put_response["creator_type"] == payload["creator_type"]
        assert put_response["app_id"] == payload["app_id"]
        ex_id = put_response["id"]
        # GET
        get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}")
        assert get_response.status_code == 200
        get_response = get_response.json()
        if i == 0:
            assert get_response["creator_id"] == creator_ids[0]
        # no new examination, receive old examination
        elif i == 1:
            assert get_response["creator_id"] == creator_ids[0]
        assert get_response["case_id"] == payload["case_id"]
        assert get_response["creator_type"] == payload["creator_type"]
        assert get_response["app_id"] == payload["app_id"]
        assert len(get_response["jobs"]) == len(put_response["jobs"])
        assert get_response["state"] == "OPEN"
