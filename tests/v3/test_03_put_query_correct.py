from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_multiple_put_query_case_id():
    case_id = str(uuid4())
    for i in range(10):
        # put
        payload = {
            "case_id": case_id if i % 2 == 0 else str(uuid4()),
            "creator_id": str(uuid4()),
            "app_id": str(uuid4()),
            "creator_type": "USER",
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201
        # we need to close the examinations as there must only be one open examination per case
        requests.put(f"{es_url}/v3/examinations/{put_response.json()['id']}/state", json={"state": "CLOSED"})

    # Test PUT query
    query = {"cases": [case_id]}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 5
    assert len(put_response["items"]) == 5


def test_put_multiple_put_query_app_id():
    app_id = str(uuid4())
    for i in range(10):
        # put
        payload = {
            "case_id": str(uuid4()),
            "creator_id": str(uuid4()),
            "app_id": app_id if i % 2 == 0 else str(uuid4()),
            "creator_type": "USER",
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201

    # Test PUT query
    query = {"apps": [app_id]}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 5
    assert len(put_response["items"]) == 5


def test_put_multiple_put_query_creator_id():
    creator_id = str(uuid4())
    for i in range(10):
        # put
        payload = {
            "case_id": str(uuid4()),
            "creator_id": creator_id if i % 2 == 0 else str(uuid4()),
            "app_id": str(uuid4()),
            "creator_type": "USER",
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201

    # Test PUT query
    query = {"creators": [creator_id]}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 5
    assert len(put_response["items"]) == 5


def test_put_multiple_put_query_creator_id_no_results():
    creator_id = str(uuid4())
    # Test PUT query
    query = {"creators": [creator_id]}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 0
    assert len(put_response["items"]) == 0


def test_put_multiple_put_query_creator_type_and_creator_id():
    creator_id = str(uuid4())
    for i in range(10):
        # put
        payload = {
            "case_id": str(uuid4()),
            "creator_id": creator_id if i < 2 else str(uuid4()),
            "creator_type": "USER" if i % 2 == 0 else "SERVICE",
            "app_id": str(uuid4()),
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201
    # Test PUT query
    query = {"creator_types": ["USER"], "creators": [creator_id]}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 1
    assert len(put_response["items"]) == 1
    query = {"creator_types": ["SERVICE"], "creators": [creator_id]}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 1
    assert len(put_response["items"]) == 1


def test_put_multiple_put_query_creator_type_and_creator_id_skip_limit():
    creator_id = str(uuid4())
    for i in range(100):
        # put
        payload = {
            "case_id": str(uuid4()),
            "creator_id": creator_id,
            "creator_type": "USER" if i % 2 == 0 else "SERVICE",
            "app_id": str(uuid4()),
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201
    # Test PUT query skip & limit
    params = {"skip": 10, "limit": 20}
    query = {"creator_types": ["USER"], "creators": [creator_id]}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query, params=params).json()
    assert put_response["item_count"] == 50
    assert len(put_response["items"]) == 20


# #####################
# ## jobs and scope ###
# #####################


def test_put_multiple_put_query_jobs_no_results():
    # Test PUT query
    query = {
        "jobs": [str(uuid4()), str(uuid4())],
    }
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 0
    assert len(put_response["items"]) == 0


def test_put_multiple_put_query_scopes_no_results():
    # Test PUT query
    query = {
        "scopes": [str(uuid4()), str(uuid4())],
    }
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 0
    assert len(put_response["items"]) == 0


def test_put_multiple_put_query_jobs_and_scopes_no_results():
    # Test PUT query
    query = {
        "jobs": [str(uuid4()), str(uuid4())],
        "scopes": [str(uuid4()), str(uuid4())],
    }
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 0
    assert len(put_response["items"]) == 0


def test_put_multiple_put_query_jobs_and_scopes():
    job_ids = []
    scope_ids = []
    n_ex = 5
    n_jobs_per_ex = 3
    for _ in range(n_ex):
        # put ex
        payload = {
            "case_id": str(uuid4()),
            "creator_id": str(uuid4()),
            "creator_type": "USER",
            "app_id": str(uuid4()),
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201
        ex = put_response.json()
        # put scope
        payload = {
            "examination_id": ex["id"],
            "user_id": ex["creator_id"],
        }
        put_response = requests.put(f"{es_url}/v3/scopes", json=payload)
        print(put_response.text)
        assert put_response.status_code == 201
        scope = put_response.json()
        scope_ids.append(scope["id"])

        # put n jobs
        for _ in range(n_jobs_per_ex):
            job_id = str(uuid4())
            put_response = requests.put(f"{es_url}/v3/examinations/{ex['id']}/jobs/{job_id}/add")
            print(put_response.text)
            assert put_response.status_code == 201
            _ex = put_response.json()
            job_ids.append(job_id)

    # Test PUT query jobs
    query = {"jobs": job_ids}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == n_ex
    assert len(put_response["items"]) == n_ex
    # Test PUT query scopes
    query = {"scopes": scope_ids}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == n_ex
    assert len(put_response["items"]) == n_ex
    # Test PUT query jobs + scopes #1
    query = {"scopes": scope_ids, "jobs": job_ids}
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == n_ex
    assert len(put_response["items"]) == n_ex
    # Test PUT query jobs + scopes #2
    query = {
        "scopes": [scope_ids[0]],
        "jobs": job_ids,
    }
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 1
    assert len(put_response["items"]) == 1
    # Test PUT query jobs + scopes #3
    query = {
        "scopes": scope_ids,
        "jobs": [job_ids[0]],
    }
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 1
    assert len(put_response["items"]) == 1
    # Test PUT query jobs + scopes #3
    query = {
        "scopes": [scope_ids[1]],
        "jobs": [job_ids[0]],
    }
    put_response = requests.put(f"{es_url}/v3/examinations/query", json=query).json()
    assert put_response["item_count"] == 0
    assert len(put_response["items"]) == 0
