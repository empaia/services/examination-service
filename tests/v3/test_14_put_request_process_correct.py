import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_preprocessing_request_update():
    creator_id = str(uuid4())
    slide_id = str(uuid4())
    payload = {
        "creator_id": creator_id,
        "creator_type": "USER",
        "slide_id": slide_id,
    }
    response = requests.post(f"{es_url}/v3/preprocessing-requests", json=payload)
    assert response.status_code == 201
    request_id = response.json()["id"]

    time.sleep(2)
    update = {
        "app_jobs": [
            {
                "job_id": str(uuid4()),
                "app_id": str(uuid4()),
                "trigger_id": str(uuid4()),
            },
            {
                "job_id": str(uuid4()),
                "app_id": str(uuid4()),
                "trigger_id": str(uuid4()),
            },
        ],
    }
    response = requests.put(f"{es_url}/v3/preprocessing-requests/{request_id}/process", json=update)
    assert response.status_code == 200

    response = requests.get(f"{es_url}/v3/preprocessing-requests/{request_id}")
    pp_request = response.json()
    assert pp_request["creator_id"] == creator_id
    assert pp_request["creator_type"] == "USER"
    assert pp_request["slide_id"] == slide_id
    assert pp_request["app_jobs"] == update["app_jobs"]
    assert pp_request["state"] == "PROCESSED"
    assert pp_request["created_at"] < pp_request["updated_at"]

    response = requests.put(f"{es_url}/v3/preprocessing-requests/{request_id}/process", json=update)
    assert response.status_code == 409
