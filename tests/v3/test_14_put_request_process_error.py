from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_not_existent_preprocessing_request_update():
    request_id = str(uuid4())
    update = {
        "app_jobs": [
            {
                "job_id": str(uuid4()),
                "app_id": str(uuid4()),
                "trigger_id": str(uuid4()),
            },
        ],
    }
    response = requests.put(f"{es_url}/v3/preprocessing-requests/{request_id}/process", json=update)
    assert response.status_code == 404


def test_put_same_job_twice_into_preprocessing_request_update():
    payload = {
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "slide_id": str(uuid4()),
    }
    response = requests.post(f"{es_url}/v3/preprocessing-requests", json=payload)
    assert response.status_code == 201
    request_id = response.json()["id"]

    job_id = str(uuid4())
    app_id = str(uuid4())
    update = {
        "app_jobs": [
            {
                "job_id": job_id,
                "app_id": app_id,
                "trigger_id": str(uuid4()),
            },
            {
                "job_id": job_id,
                "app_id": app_id,
                "trigger_id": str(uuid4()),
            },
        ],
    }
    response = requests.put(f"{es_url}/v3/preprocessing-requests/{request_id}/process", json=update)
    assert response.status_code == 409
