from uuid import uuid4

import requests

from ..singletons import es_url
from .helpers import ExaminationHelpers


def test_query_scopes():
    user_id = str(uuid4())
    user_id_2 = str(uuid4())
    case_id = str(uuid4())
    app_id = str(uuid4())
    app_id_2 = str(uuid4())

    # Create examinations
    examination_data = ExaminationHelpers.create_or_get_examination(case_id, user_id, app_id)
    ex_id = examination_data["id"]

    examination_data = ExaminationHelpers.create_or_get_examination(case_id, user_id, app_id_2)
    ex_id_2 = examination_data["id"]

    # Create scopes
    # Ex 1 / User 1
    scope_data = ExaminationHelpers.create_or_get_scope(ex_id, user_id, 201)
    assert "id" in scope_data
    assert scope_data["examination_id"] == ex_id
    assert scope_data["user_id"] == user_id
    scope_id_1_1 = scope_data["id"]

    # Ex 1 / User 2
    scope_data = ExaminationHelpers.create_or_get_scope(ex_id, user_id_2, 201)
    assert "id" in scope_data
    assert scope_data["examination_id"] == ex_id
    assert scope_data["user_id"] == user_id_2
    scope_id_1_2 = scope_data["id"]

    # Ex 2 / User 1
    scope_data = ExaminationHelpers.create_or_get_scope(ex_id_2, user_id, 201)
    assert "id" in scope_data
    assert scope_data["examination_id"] == ex_id_2
    assert scope_data["user_id"] == user_id
    scope_id_2_1 = scope_data["id"]

    # Ex 2 / User 2
    scope_data = ExaminationHelpers.create_or_get_scope(ex_id_2, user_id_2, 201)
    assert "id" in scope_data
    assert scope_data["examination_id"] == ex_id_2
    assert scope_data["user_id"] == user_id_2
    scope_id_2_2 = scope_data["id"]

    # Query scopes - no filter
    query = {}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] > 0
    assert len(scopes["items"]) > 0

    # Query scopes - single ex
    query = {"examinations": [ex_id]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 2
    assert len(scopes["items"]) == 2

    # Query scopes - two ex
    query = {"examinations": [ex_id, ex_id_2]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 4
    assert len(scopes["items"]) == 4

    # Query scopes - single scope
    query = {"scopes": [scope_id_1_1]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 1
    assert len(scopes["items"]) == 1

    # Query scopes - all scopes
    query = {"scopes": [scope_id_1_1, scope_id_1_2, scope_id_2_1, scope_id_2_2]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 4
    assert len(scopes["items"]) == 4

    # Query scopes - single ex, single scope
    query = {"examinations": [ex_id], "scopes": [scope_id_1_1]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 1
    assert len(scopes["items"]) == 1

    # Query scopes - unknown ex
    query = {"examinations": [str(uuid4())]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 0
    assert len(scopes["items"]) == 0

    # Query scopes - unknown scope
    query = {"scopes": [str(uuid4())]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 0
    assert len(scopes["items"]) == 0

    # Query scopes - valid ex, unknown scope
    query = {"examinations": [ex_id], "scopes": [str(uuid4())]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 0
    assert len(scopes["items"]) == 0

    # Query scopes - unknown ex, valid scope
    query = {"examinations": [str(uuid4())], "scopes": [scope_id_1_1]}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query).json()
    assert scopes["item_count"] == 0
    assert len(scopes["items"]) == 0

    # Query scopes - limit
    query = {"scopes": [scope_id_1_1, scope_id_1_2, scope_id_2_1, scope_id_2_2]}
    params = {"limit": 1}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query, params=params).json()
    assert scopes["item_count"] > 0
    assert len(scopes["items"]) == 1

    # Query scopes - skip
    query = {"scopes": [scope_id_1_1, scope_id_1_2, scope_id_2_1, scope_id_2_2]}
    params = {"skip": 5000}
    scopes = requests.put(f"{es_url}/v3/scopes/query", json=query, params=params).json()
    assert scopes["item_count"] > 0
    assert len(scopes["items"]) == 0
