from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_add_job_closed_ex():
    # put examination
    app_id = str(uuid4())
    case_id = str(uuid4())
    payload = {
        "case_id": case_id,
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": app_id,
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    put_response = put_response.json()
    ex_id = put_response["id"]

    # PUT state
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200

    # PUT Job
    job_id = str(uuid4())
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id}/add")
    assert put_response.status_code == 423


def test_put_add_job_invalid_ex_id():
    put_response = requests.put(f"{es_url}/v3/examinations/{str(uuid4())}/jobs/{str(uuid4())}/add")
    assert put_response.status_code == 404
