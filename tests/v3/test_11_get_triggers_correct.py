from uuid import uuid4

import requests

from ..singletons import es_url


def test_get_triggers():
    payload = {
        "portal_app_id": str(uuid4()),
        "tissue": "LUNG",
        "stain": "HE",
        "creator_id": str(uuid4()),
        "creator_type": "USER",
    }
    response = requests.post(f"{es_url}/v3/preprocessing-triggers", json=payload)
    test_trigger = response.json()
    response = requests.get(f"{es_url}/v3/preprocessing-triggers")
    assert response.status_code == 200
    preprocessing_trigger_list = response.json()
    assert "item_count" in preprocessing_trigger_list
    assert "items" in preprocessing_trigger_list
    assert test_trigger in preprocessing_trigger_list["items"]


def test_get_single_trigger():
    payload = {
        "portal_app_id": str(uuid4()),
        "tissue": "LUNG",
        "stain": "HE",
        "creator_id": str(uuid4()),
        "creator_type": "USER",
    }
    response = requests.post(f"{es_url}/v3/preprocessing-triggers", json=payload)
    posted_trigger = response.json()
    response = requests.get(f"{es_url}/v3/preprocessing-triggers/{posted_trigger['id']}")
    assert response.status_code == 200
    trigger = response.json()
    assert trigger == posted_trigger
