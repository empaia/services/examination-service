from uuid import uuid4

import requests

from ..singletons import es_url


def test_post_preprocessing_request():
    creator_id = str(uuid4())
    slide_id = str(uuid4())
    payload = {
        "creator_id": creator_id,
        "creator_type": "USER",
        "slide_id": slide_id,
    }
    response = requests.post(f"{es_url}/v3/preprocessing-requests", json=payload)
    assert response.status_code == 201
    pp_request = response.json()
    assert pp_request["creator_id"] == creator_id
    assert pp_request["creator_type"] == "USER"
    assert pp_request["slide_id"] == slide_id
    assert pp_request["app_jobs"] == []
    assert pp_request["state"] == "OPEN"
    assert pp_request["created_at"] == pp_request["updated_at"] and pp_request["created_at"] > 0

    response = requests.get(f"{es_url}/v3/preprocessing-requests/{pp_request['id']}")
    assert response.status_code == 200
    pp_request_get = response.json()
    assert pp_request_get == pp_request
