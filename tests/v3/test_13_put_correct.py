from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_examination():
    # put new open examination
    payload = {
        "case_id": str(uuid4()),
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": str(uuid4()),
    }
    response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert response.status_code == 201
    examination = response.json()
    assert examination["case_id"] == payload["case_id"]
    assert examination["creator_id"] == payload["creator_id"]
    assert examination["creator_type"] == payload["creator_type"]
    assert examination["app_id"] == payload["app_id"]
    assert examination["state"] == "OPEN"

    # ensure persistence
    examination_id = examination["id"]
    response = requests.get(f"{es_url}/v3/examinations/{examination_id}")
    assert response.status_code == 200
    examination = response.json()
    assert examination["case_id"] == payload["case_id"]
    assert examination["creator_id"] == payload["creator_id"]
    assert examination["creator_type"] == payload["creator_type"]
    assert examination["app_id"] == payload["app_id"]
    assert examination["state"] == "OPEN"

    # ensure idempotence
    response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert response.status_code == 200
    examination = response.json()
    assert examination["id"] == examination_id
    assert examination["case_id"] == payload["case_id"]
    assert examination["creator_id"] == payload["creator_id"]
    assert examination["creator_type"] == payload["creator_type"]
    assert examination["app_id"] == payload["app_id"]
    assert examination["state"] == "OPEN"

    # ensure idempotence even with different creator
    original_creator_id = payload["creator_id"]
    original_creator_type = payload["creator_type"]
    payload = {
        "case_id": payload["case_id"],
        "creator_id": str(uuid4()),
        "creator_type": "SERVICE",
        "app_id": payload["app_id"],
    }
    response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert response.status_code == 200
    examination = response.json()
    assert examination["id"] == examination_id
    assert examination["case_id"] == payload["case_id"]
    assert examination["creator_id"] == original_creator_id
    assert examination["creator_type"] == original_creator_type
    assert examination["app_id"] == payload["app_id"]
    assert examination["state"] == "OPEN"

    # after closing this examination, put will create a new one
    response = requests.put(f"{es_url}/v3/examinations/{examination_id}/state", json={"state": "CLOSED"})
    assert response.status_code == 200
    response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert response.status_code == 201
    examination = response.json()
    assert examination["id"] != examination_id
    assert examination["case_id"] == payload["case_id"]
    assert examination["creator_id"] == payload["creator_id"]
    assert examination["creator_type"] == payload["creator_type"]
    assert examination["app_id"] == payload["app_id"]
    assert examination["state"] == "OPEN"
