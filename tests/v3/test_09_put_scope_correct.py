import asyncio
import time
from uuid import uuid4

import asyncpg
import jwt
from fastapi.exceptions import HTTPException

from examination_service.api.v3.clients.db_clients import db_clients
from examination_service.singletons import settings

from .helpers import Assert, ExaminationHelpers


async def _test_scope_uniqueness():
    user_id = str(uuid4())
    case_id = str(uuid4())
    app_id = str(uuid4())

    # Create examination
    examination_data = ExaminationHelpers.create_or_get_examination(case_id, user_id, app_id)
    examination_id = examination_data["id"]

    pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
    async with pool.acquire() as conn:
        clients = await db_clients(conn)
        # Initial insert must succeed
        scope = await clients.scope.create_scope(examination_id, user_id)  # pylint: disable=W0212
        assert scope is not None
        assert str(scope.examination_id) == examination_id
        assert str(scope.user_id) == user_id
        assert str(scope.app_id) == app_id

        # Test that the scope actually exists
        get_scope = await clients.scope.get_scope(examination_id, user_id)  # pylint: disable=W0212
        assert scope.id == get_scope.id
        assert scope.examination_id == get_scope.examination_id
        assert scope.user_id == get_scope.user_id
        assert scope.app_id == get_scope.app_id

        # Second insert must do nothing
        raw_scope = await clients.scope._insert_scope(examination_id, user_id)  # pylint: disable=W0212
        assert raw_scope is None

        # Also test _add_scope(), it calls _insert_scope internally and catches Postgres exceptions
        try:
            await clients.scope._add_scope(examination_id, user_id)  # pylint: disable=W0212
        except HTTPException as e:
            assert e.status_code == 400
        else:
            raise AssertionError("_add_scope() did not raise 400 after attempt to create a scope a second time")

        # Finally test that the scope still exists
        get_scope = await clients.scope.get_scope(examination_id, user_id)  # pylint: disable=W0212
        assert scope.id == get_scope.id
        assert scope.examination_id == get_scope.examination_id
        assert scope.user_id == get_scope.user_id
        assert scope.app_id == get_scope.app_id


def test_scope_uniqueness():
    asyncio.run(_test_scope_uniqueness())


def test_get_or_create_scope():
    user_id = str(uuid4())
    user_id_2 = str(uuid4())
    case_id = str(uuid4())
    app_id = str(uuid4())

    # Create examination
    examination_data = ExaminationHelpers.create_or_get_examination(case_id, user_id, app_id)
    examination_id = examination_data["id"]

    # Create scope
    scope_data = ExaminationHelpers.create_or_get_scope(examination_id, user_id, 201)
    assert "id" in scope_data
    assert scope_data["examination_id"] == examination_id
    assert scope_data["user_id"] == user_id
    scope_id = scope_data["id"]

    # Put scope again
    scope_data_again = ExaminationHelpers.create_or_get_scope(examination_id, user_id, 200)
    assert scope_data_again["id"] == scope_id
    assert scope_data_again["examination_id"] == examination_id
    assert scope_data_again["user_id"] == user_id

    # Put scope again different user (equals new scope)
    scope_data_again = ExaminationHelpers.create_or_get_scope(examination_id, user_id_2, 201)
    assert scope_data_again["id"] != scope_id
    assert scope_data_again["examination_id"] == examination_id
    assert scope_data_again["user_id"] != user_id

    # Get scope data by scope_id
    scope_data = ExaminationHelpers.get_scope_data(scope_id)
    assert scope_data["examination_id"] == examination_id
    assert scope_data["user_id"] == user_id
    assert (time.time() - scope_data["created_at"]) < 1

    # Verify that the data is the same
    scope_data_again = ExaminationHelpers.get_scope_data(scope_id)
    Assert.dicts_equal(scope_data, scope_data_again)

    # Verify that the access token is valid
    scope_token_data = ExaminationHelpers.get_scope_token(scope_id)
    public_key = ExaminationHelpers.get_public_key()
    assert b"PRIVATE KEY" not in public_key
    assert b"BEGIN RSA PUBLIC KEY" in public_key
    payload = jwt.decode(scope_token_data["access_token"], key=public_key, algorithms="RS256")
    assert payload["sub"] == scope_id
    scope_token_data_again = ExaminationHelpers.get_scope_token(scope_id)
    payload2 = jwt.decode(scope_token_data_again["access_token"], key=public_key, algorithms="RS256")
    assert payload2["sub"] == scope_id

    # Test that no token is returned for an invalid scope id
    ExaminationHelpers.get_scope_token(str(uuid4()), expected_status_code=404)
