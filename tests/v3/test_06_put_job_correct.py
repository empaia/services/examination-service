import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_add_job_app_get():
    # put examination
    app_id = str(uuid4())
    case_id = str(uuid4())
    payload = {
        "case_id": case_id,
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": app_id,
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    put_response = put_response.json()
    ex_id = put_response["id"]
    last_update = put_response["updated_at"]

    for i in range(3):
        time.sleep(2)
        # PUT Job
        job_id = str(uuid4())
        put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id}/add")
        assert put_response.status_code == 201
        # PUT twice, should return 200
        put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id}/add")
        assert put_response.status_code == 200
        put_response = put_response.json()
        assert put_response["id"] == ex_id
        assert put_response["app_id"] == app_id
        assert job_id in put_response["jobs"]
        assert last_update < put_response["updated_at"]
        last_update = put_response["updated_at"]

        # GET job for examination
        get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}").json()
        assert len(get_response["jobs"]) == i + 1

    get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}", json=payload).json()
    assert len(get_response["jobs"]) == 3
