import time
from uuid import uuid4

import requests

from ..singletons import es_url


def test_get_examination_id_by_job_id():
    # put examination
    app_id = str(uuid4())
    case_id = str(uuid4())
    payload = {
        "case_id": case_id,
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": app_id,
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    put_response = put_response.json()
    ex_id = put_response["id"]
    updated_at = put_response["updated_at"]

    # PUT Job
    job_id_1 = str(uuid4())
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id_1}/add")
    assert put_response.status_code == 201
    assert len(put_response.json()["jobs"]) == 1
    job_id_2 = str(uuid4())
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id_2}/add")
    assert put_response.status_code == 201
    assert len(put_response.json()["jobs"]) == 2

    # DELETE Job
    time.sleep(2)
    put_response = requests.delete(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id_1}")
    assert put_response.status_code == 200
    ex = put_response.json()
    assert len(ex["jobs"]) == 1
    assert updated_at < ex["updated_at"]

    put_response = requests.delete(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id_1}")
    assert put_response.status_code == 404

    # CLOSE ex
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200

    # DELETE Job
    put_response = requests.delete(f"{es_url}/v3/examinations/{ex_id}/jobs/{job_id_2}")
    assert put_response.status_code == 423

    get_response = requests.get(f"{es_url}/v3/examinations/{ex_id}").json()
    assert len(get_response["jobs"]) == 1
