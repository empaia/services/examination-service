import difflib
import json

import requests

from ..singletons import es_url


class Compare:
    @staticmethod
    def status_codes_equal(response, expected_code):
        return response.status_code == expected_code

    @staticmethod
    def dicts_equal(expected_dict, actual_dict):
        expected_string = json.dumps(expected_dict, sort_keys=True, indent=2)
        actual_string = json.dumps(actual_dict, sort_keys=True, indent=2)
        return Compare.strings_equal(expected_string, actual_string)

    @staticmethod
    def strings_equal(expected_string, actual_string):
        return expected_string == actual_string


class Assert:
    @staticmethod
    def status_code_equal(response, expected_code):
        if not Compare.status_codes_equal(response, expected_code):
            msg = (
                f"Expected response status {expected_code}, but got {response.status_code} instead:\n"
                f"{json.dumps(response.json(), indent=2)}"
            )
            raise AssertionError(msg)

    @staticmethod
    def dicts_equal(expected_dict, actual_dict):
        expected_string = json.dumps(expected_dict, sort_keys=True, indent=2)
        actual_string = json.dumps(actual_dict, sort_keys=True, indent=2)
        Assert.strings_equal(expected_string, actual_string)

    @staticmethod
    def strings_equal(expected_string, actual_string):
        if not Compare.strings_equal(expected_string, actual_string):
            expected_lines = expected_string.splitlines(1)
            actual_lines = actual_string.splitlines(1)
            msg = f"Items are not equal:\n{''.join(difflib.unified_diff(expected_lines, actual_lines))}"
            raise AssertionError(msg)


class ExaminationHelpers:
    @staticmethod
    def create_or_get_examination(case_id, user_id, app_id, expected_status_code=201):
        payload = {
            "case_id": case_id,
            "creator_id": user_id,
            "creator_type": "USER",
            "app_id": app_id,
        }
        response = requests.put(f"{es_url}/v3/examinations", json=payload)
        Assert.status_code_equal(response, expected_status_code)
        return response.json()

    @staticmethod
    def close_examination(examination_id, expected_status_code=200):
        payload = {"state": "CLOSED"}
        response = requests.put(f"{es_url}/v3/examinations/{examination_id}/state", json=payload)
        Assert.status_code_equal(response, expected_status_code)
        return response.json()

    @staticmethod
    def create_or_get_scope(examination_id, user_id, expected_status_code=201):
        response = requests.put(
            f"{es_url}/v3/scopes",
            json={
                "examination_id": examination_id,
                "user_id": user_id,
            },
        )
        print(response.text, response.status_code)
        Assert.status_code_equal(response, expected_status_code)
        return response.json()

    @staticmethod
    def get_scope_data(scope_id, expected_status_code=200):
        response = requests.get(f"{es_url}/v3/scopes/{scope_id}")
        Assert.status_code_equal(response, expected_status_code)
        return response.json()

    @staticmethod
    def get_scope_token(scope_id, expected_status_code=200):
        response = requests.get(f"{es_url}/v3/scopes/{scope_id}/token")
        Assert.status_code_equal(response, expected_status_code)
        return response.json()

    @staticmethod
    def get_public_key(expected_status_code=200):
        response = requests.get(f"{es_url}/v3/public-key")
        Assert.status_code_equal(response, expected_status_code)
        return response.json().encode("ascii")
