from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_update_state_close():
    # PUT state
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v3/examinations/{uuid4()}/state", json=update_payload)
    assert put_response.status_code == 404


def test_put_update_state_close_to_open():
    # put
    payload = {
        "case_id": str(uuid4()),
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": str(uuid4()),
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    put_response = put_response.json()
    ex_id = put_response["id"]
    # PUT state
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200
    # PUT state
    update_payload = {"state": "OPEN"}
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200


def test_put_update_state_close_put_new_open():
    # put
    app_id = str(uuid4())
    case_id = str(uuid4())
    payload = {
        "case_id": case_id,
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": app_id,
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    put_response = put_response.json()
    ex_id = put_response["id"]
    # PUT state
    update_payload = {"state": "CLOSED"}
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 200
    # put
    payload = {
        "case_id": case_id,
        "creator_id": str(uuid4()),
        "creator_type": "USER",
        "app_id": app_id,
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 201
    assert put_response.json()["state"] == "OPEN"
    # PUT state
    update_payload = {"state": "OPEN"}
    put_response = requests.put(f"{es_url}/v3/examinations/{ex_id}/state", json=update_payload)
    assert put_response.status_code == 409
