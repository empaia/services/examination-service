from uuid import uuid4

import requests

from ..singletons import es_url


def test_post_invalid_preprocessing_request():
    payload = {"creator_id": str(uuid4()), "creator_type": "InvalidType", "slide_id": str(uuid4())}
    response = requests.post(f"{es_url}/v3/preprocessing-requests", json=payload)
    assert response.status_code == 422


def test_post_preprocessing_request_without_slide():
    payload = {"creator_id": str(uuid4()), "creator_type": "USER"}
    response = requests.post(f"{es_url}/v3/preprocessing-requests", json=payload)
    assert response.status_code == 422


def test_get_nonexistent_preprocessing_request():
    response = requests.get(f"{es_url}/v3/preprocessing-requests/{str(uuid4())}")
    assert response.status_code == 404
