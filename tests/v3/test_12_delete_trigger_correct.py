from uuid import uuid4

import requests

from ..singletons import es_url


def test_delete_trigger():
    payload = {
        "portal_app_id": str(uuid4()),
        "tissue": "LUNG",
        "stain": "HE",
        "creator_id": str(uuid4()),
        "creator_type": "USER",
    }
    response = requests.post(f"{es_url}/v3/preprocessing-triggers", json=payload)
    posted_trigger = response.json()
    response = requests.delete(f"{es_url}/v3/preprocessing-triggers/{posted_trigger['id']}")
    assert response.status_code == 204
    response = requests.get(f"{es_url}/v3/preprocessing-triggers/{posted_trigger['id']}")
    assert response.status_code == 404
