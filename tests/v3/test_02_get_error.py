from uuid import uuid4

import requests

from ..singletons import es_url


def test_get_wrong_id_examination():
    # Test GET wrong ID
    get_response = requests.get(f"{es_url}/v3/examinations/{uuid4()}")
    assert get_response.status_code == 404


def test_put_multiple_get_all_examinations_skip_limit():
    for _ in range(10):
        # put
        payload = {
            "case_id": str(uuid4()),
            "creator_id": str(uuid4()),
            "creator_type": "USER",
            "app_id": str(uuid4()),
        }
        put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
        assert put_response.status_code == 201

    count = requests.get(f"{es_url}/v3/examinations").json()["item_count"]
    # Test GET all skip & limit way higher then items exist
    params = {"skip": 5000, "limit": 2000}
    get_response = requests.get(f"{es_url}/v3/examinations", params=params).json()
    assert get_response["item_count"] == count
    assert len(get_response["items"]) == 0
