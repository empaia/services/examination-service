from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_examination():
    # put
    payload = {
        "case_id": str(uuid4()),
        "creator_id": str(uuid4()),
        "creator_type": "iNvaLId TyPe",
        "app_id": str(uuid4()),
        "state": "iNvaLId TyPe",
    }
    put_response = requests.put(f"{es_url}/v3/examinations", json=payload)
    assert put_response.status_code == 422
