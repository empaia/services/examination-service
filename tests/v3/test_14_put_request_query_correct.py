from uuid import uuid4

import requests

from ..singletons import es_url


def test_put_preprocessing_request_query():
    creator_id = str(uuid4())
    slide_id = str(uuid4())
    payload = {
        "creator_id": creator_id,
        "creator_type": "USER",
        "slide_id": slide_id,
    }
    response = requests.post(f"{es_url}/v3/preprocessing-requests", json=payload)
    assert response.status_code == 201
    request_id_1 = response.json()["id"]
    response = requests.post(f"{es_url}/v3/preprocessing-requests", json=payload)
    assert response.status_code == 201

    query = {}
    response = requests.put(f"{es_url}/v3/preprocessing-requests/query", json=query)
    assert response.status_code == 200
    result = response.json()
    original_all_requests_count = result["item_count"]
    assert original_all_requests_count >= 2  # other tests might have added other requests

    query = {"states": ["OPEN"]}
    response = requests.put(f"{es_url}/v3/preprocessing-requests/query", json=query)
    assert response.status_code == 200
    result = response.json()
    original_open_requests_count = result["item_count"]
    assert original_open_requests_count <= original_all_requests_count

    # other tests might have added other OPEN requests
    items = [item for item in result["items"] if item["slide_id"] == slide_id]
    assert len(items) == 2
    for item in items:
        assert item["creator_id"] == creator_id
        assert item["creator_type"] == "USER"
        assert item["slide_id"] == slide_id
        assert item["app_jobs"] == []
        assert item["state"] == "OPEN"
        assert item["created_at"] == item["updated_at"] and item["created_at"] > 0

    query = {"states": ["PROCESSED"]}
    response = requests.put(f"{es_url}/v3/preprocessing-requests/query", json=query)
    assert response.status_code == 200
    result = response.json()
    original_processed_requests_count = result["item_count"]
    assert original_all_requests_count == original_processed_requests_count + original_open_requests_count

    update = {"app_jobs": []}
    response = requests.put(f"{es_url}/v3/preprocessing-requests/{request_id_1}/process", json=update)
    assert response.status_code == 200

    query = {"states": ["PROCESSED"]}
    response = requests.put(f"{es_url}/v3/preprocessing-requests/query", json=query)
    assert response.status_code == 200
    result = response.json()
    processed_requests_count = result["item_count"]
    assert processed_requests_count == original_processed_requests_count + 1

    query = {"states": ["OPEN"]}
    response = requests.put(f"{es_url}/v3/preprocessing-requests/query", json=query)
    assert response.status_code == 200
    result = response.json()
    open_requests_count = result["item_count"]
    assert open_requests_count == original_open_requests_count - 1

    query = {}
    response = requests.put(f"{es_url}/v3/preprocessing-requests/query", json=query)
    assert response.status_code == 200
    result = response.json()
    all_requests_count = result["item_count"]
    assert all_requests_count == original_all_requests_count
