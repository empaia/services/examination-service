from uuid import uuid4

import requests

from ..singletons import es_url


def test_post_trigger_with_same_info_twice():
    payload = {
        "portal_app_id": str(uuid4()),
        "tissue": "LUNG",
        "stain": "HE",
        "creator_id": str(uuid4()),
        "creator_type": "USER",
    }
    post_response = requests.post(f"{es_url}/v3/preprocessing-triggers", json=payload)
    assert post_response.status_code == 201
    post_response = requests.post(f"{es_url}/v3/preprocessing-triggers", json=payload)
    assert post_response.status_code == 409
    payload["creator_id"] = str(uuid4())
    post_response = requests.post(f"{es_url}/v3/preprocessing-triggers", json=payload)
    assert post_response.status_code == 409
