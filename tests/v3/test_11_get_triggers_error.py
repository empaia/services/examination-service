from uuid import uuid4

import requests

from ..singletons import es_url


def test_get_trigger_with_wrong_id():
    invalid_id = str(uuid4())
    response = requests.get(f"{es_url}/v3/preprocessing-triggers/{invalid_id}")
    assert response.status_code == 404
