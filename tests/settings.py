from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    es_url: str
    enable_auth: bool = False

    model_config = SettingsConfigDict(env_file=".env", env_prefix="PYTEST_", extra="ignore")
