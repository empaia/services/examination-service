# Changelog

## 0.8.26 - 41

* renovate

## 0.8.25

* updated RSA keys dir for docker compose

## 0.8.24

* renovate

## 0.8.23

* added scope query endpoint in v3
* bug fix in examination query
* removed endpoint `/jobs/{job_id}/examinations`

## 0.8.16 - 22

* renovate

## 0.8.15

* update models repo and pinned pydantic version

## 0.8.14

* renovate

## 0.8.13

* renovate

## 0.8.12

* renovate

## 0.8.11

* renovate

## 0.8.10

* renovate

## 0.8.9

* renovate

## 0.8.8

* renovate

## 0.8.7

* renovate

## 0.8.6

* renovate

## 0.8.5

* renovate

## 0.8.4

* renovate

## 0.8.3

* renovate

## 0.8.2

* renovate

## 0.8.1

* updated models repo

## 0.7.34 & 0.8.0

* migrate to pydantic v2
* default values for settings

## 0.7.33

* renovate


## 0.7.32

* renovate

## 0.7.31

* renovate

## 0.7.30

* renovate

## 0.7.29

* renovate

## 0.7.28

* renovate

## 0.7.27

* renovate

## 0.7.26

* renovate

## 0.7.25

* renovate

## 0.7.24

* renovate


## 0.7.23

* renovate

## 0.7.22

* renovate

## 0.7.21

* renovate

## 0.7.20

* renovate

## 0.7.19

* renovate

## 0.7.18

* renovate

## 0.7.17

* renovate

## 0.7.16

* renovate

## 0.7.15

* renovate

## 0.7.14

* separate key-pairs per api version

## 0.7.13

* renovate

## 0.7.12

* renovate

## 0.7.11

* renovate

## 0.7.10

* added app_id to Scope model (only for returned Scope, not PostScope)
* added scopes and jobs as query parameters for emanaiations/query endpoint

## 0.7.9

* bugfix for empty preprocessing trigger list

## 0.7.8

* renovate

## 0.7.7

* renovate

## 0.7.6

* renovate

## 0.7.5

* renamed app ui state to `storage`

## 0.7.4

* added app ui states (scope and user)

## 0.7.3

* renovate

## 0.7.2

* `v3` removed post examination and post scope (use put instead)

## 0.7.1

* renovate

## 0.7.0

* API versioning restructuring
  * api versions `v1` and `v3`
  * tests for `v1` and `v3`
* post /examinations for `v1` throws 409 if there is already an open examination for the specified case
* post /examinations for `v3` throws 409 if there is already an open examination for the specified case / app pair
  * enforced by db constraint.
* `v3` 1 open case per case-app pair
* `v3` 1 app per examinaiton only, specified at examination creation
* `v3` removed app_id from scope
* `v3` added `/preprocessing-triggers` and `/preprocessing-requests` endpoints for `v3`

## 0.6.40

* renovate

## 0.6.39

* renovate

## 0.6.38

* renovate

## 0.6.37

* renovate

## 0.6.36

* renovate

## 0.6.35

* renovate

## 0.6.34

* renovate

## 0.6.33

* renovate

## 0.6.32

* renovate

## 0.6.31

* renovate

## 0.6.30

* changed renovate responsibility

## 0.6.29

* renovate

## 0.6.28

* renovate

## 0.6.27

* renovate

## 0.6.26

* renovate

## 0.6.25

* renovate

## 0.6.24

* renovate

## 0.6.23

* renovate

## 0.6.22

* renovate

## 0.6.21

* updated dependencies

## 0.6.20

* updated dependencies

## 0.6.19

* updated ci

## 0.6.18

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

## 0.6.17

* allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly
use an authorization header)

## 0.6.16

* updated models submodule for recent changes of class AccessTokenTools class
* create rsa keys for job token creation before starting the fastapi app, because otherwise there will be a race
condition if parallel fastapi worker processes are used and the worker processes might use different rsa keys.

# 0.6.15

* added env variable `ES_SCOPE_TOKEN_EXP`

# 0.6.14

* added ID validation

# 0.6.13

* added route /v1/scopes/{scope_id}/token to retrieve a new scope access token 
* replaced route /v1/examinations/{examination_id}/apps/{app_id}/scope by PUT and POST routes /v1/scopes

# 0.6.12

* added route to allow access to the public key for scope access token validation

# 0.6.11

* adapted examination-service to refactored scope access token code in the models repo

# 0.6.10

* removed scope access token validation, which will move to the workbench-service

# 0.6.9

* added route /v1/scopes/{scope_id} to retrieve scope data
* route /v1/examinations/{examination_id}/apps/{app_id}/scope returns now {scope_id, raw_token}

# 0.6.8

* added route /v1/examinations/{examination_id}/apps/{app_id}/scope to create and retrieve scope data

# 0.6.7

* catch UniqueViolationError when creating PSQL extensions, due to race conditions when sharing a DB

# 0.6.6

* added prefix for migration_steps table

# 0.6.5

* updated db-migration

# 0.6.4

* migrated from SQLAlchemy and psycopg to asyncpg
* fixed and tested order by
* updated models

# 0.6.3

* added indexes to database
* added `ORDER BY` to query sql statement

# 0.6.2

* changed response for `/alive` endpoint to include service version

# 0.6.1

* changed endpoint `GET /v1/examinations/jobs/{job_id}` to `GET /v1/jobs/{job_id}/examinations`

# 0.6.0

## 2021-07-20

* added new endpoints
  * get examination by id of registered job
    `GET /v1/examinations/jobs/{job_id}`
  * delete job from examination
    `DELETE /v1/examinations/{examination_id}/jobs/{job_id}`
* return error when adding an app or a job to a closed examination
* updated packages
* updated models

# 0.5.1

## 2021-07-01

* changed response status code for post routes from 200 to 201

# 0.5.0
## 2021-06-14
* changed POST apps/jobs to PUT route
* updated submodule models

# 0.4.0

## 2021-05-25
* implemented GET and POST job routes for examination and app id

# 0.3.2

## 2021-05-21
* fixed a bug with duplicate jobs or apps in get examination route

# 0.3.1

## 2021-05-20
* fixed a bug with duplicate jobs or apps in examination
* added better database error handling

# 0.3.0

## 2021-05-12
* added table examinations_jobs
* added /examinations/{examination_id}/apps endpoint and tests for it
* changed response of /examinations/{examination_id}/jobs endpoint
* changed query and get logic to use changed models

# 0.2.0

## 2021-04-14
* added /alive endpoint and test for it

# 0.1.1

## 2021-04-09
* added ci version check
* removed ci trigger for medical data service

# 0.1.0

## 2021-03-26
* added versioning via `from importlib.metadata import version`

## 2021-02-19
* added changelog
* added 10s retrying db connection on startup

## 2021-02-03
* DELETE response now includes `{ "id": "id-of-deleted-examination" }`
* All POST / PUT responses now are full Examination object with jobs
* added issue templates
